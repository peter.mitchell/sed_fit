import numpy as np

def Linear_Interpolation_Old(x1_in,y1_in,x2_in):
    '''Perform linear interpolation to get
    y2 values from (x1,y1) for x2 grid of points.
    If y1 has more than one dimension then ensure
    the first y1 axis corresponds to x1.'''

    #Force x1,x2 to be sorted properly 
    x1 = np.sort(x1_in) ; x2 = np.sort(x2_in)
    y1 = y1_in[np.argsort(x1_in)]
    
    if y1.shape == x1.shape:
        y2 = np.zeros_like(x2)
    else:
        y2_shape = np.append(len(x2),y1.shape[1:])
        y2 = np.zeros(y2_shape)

    for n in range(len(x1)-1):
        #Points that don't need to be interpolated
        y2[ x2 == x1[n] ] = y1[n]

        #Interpolation
        ind = (x2 > x1[n]) & (x2 < x1[n+1])
        if y2[ind].shape[0] == 1:
            y2[ind] = y1[n] + (np.reshape(x2[ind],(len(x2[ind]),1)) - x1[n])*(y1[n+1]-y1[n])/float(x1[n+1]-x1[n])

        #Slightly crazy work-around to counter ANNOYING numpy broadcasting rules
        elif y2[ind].shape[0] > 1:
            x2_shape = (x2[ind].shape[0],)
            for m in range(len(y1.shape)-1):
                x2_shape = x2_shape + (1,)
            y2[ind] = y1[n] + (np.reshape(x2[ind],x2_shape) - x1[n])*(y1[n+1]-y1[n])/float(x1[n+1]-x1[n])

    #Invert the x2 sort so that x2_in and y2_out correspond to each other.
    y2_out = y2[np.argsort(np.argsort(x2_in))]
    
    return y2_out

from scipy.interpolate import interp1d

def Linear_Interpolation(x1_in, y1_in, x2_in, bounds_error=True, fill_value=np.nan):
   
    f = interp1d(x1_in, y1_in, kind="linear",axis=0,bounds_error=bounds_error,fill_value=fill_value)

    y2_out = f(x2_in)

    return y2_out

'''a = np.arange(0,12) + 0.1; b = np.reshape(np.arange(0,144),(12,3,4)); c = np.arange(3,16)*0.3
print a.shape, b.shape, c.shape
d = Linear_Interpolation(a,b,c)
print d.shape
exit()'''

def Read_Filter_File(file_path):
    '''Read a filter.dat file.
    n_points tells you how many
    wavlength points are in each
    filter'''
    
    file = open(file_path,'r')
    data = file.readlines()

    wavelength, response = np.loadtxt(file_path, unpack = 'True', usecols = (0,1))
    n_filters = int(data[2][len(data[2])-4:len(data[2])])
    n_points = np.zeros(n_filters)
    file.close()
    file = open(file_path,'r')
    
    ind = 0
    prev_not_comment = False

    for line in file.readlines():
        not_comment = line[0] != '#'

        if (not not_comment) and prev_not_comment:
            n_points[ind] = int(line.split()[2])
            ind += 1
        prev_not_comment = not_comment
    file.close()

    return wavelength, response, n_points

  
def Read_BC03_Binary(file_path, big_endian=True, Full = True):
    '''Reads Bruzual and Charlot 2003 SSP binary files.
    Returns 1d array of ages (yr),
    1d array of wavelengths (Angstroms), (and if Full = True)
    2d array of sed values (Solar Luminosity per angstrom)
    for different wavelengths (columns) and ages (rows),
    1d array of masses (Msun) (one for each age)'''

    #Correct for problems with reading the binary file. If you get nonsensical results (i.e. really big numbers), change to big_endian = False
    if big_endian:
        uint16, uint32, uint64, float32, float64, int32 = '>u2', '>u4', '>u8', '>f4', '>f8', '>i4'
    else:
        from numpy import uint32, uint16, uint64, float32, float64, int32
    file = open(file_path,'rb')

    header_size = np.fromfile(file, uint32, 1)[0]
    n_ages = np.fromfile(file, uint32, 1)[0]  #The number of SSP spectra in the file (one for each age)
    ages = np.fromfile(file, float32, n_ages) #Array of ages (yr)
    #Various bits and pieces of information (on IMF etc) that are not important (hopefully!). This could be cleaned up with a "seek" command.
    junk = np.fromfile(file, uint32, 2 )
    iseg = np.fromfile(file, uint32, 1 )[0]
    junk = np.fromfile(file, float32, 6*iseg )
    junk = np.fromfile(file, float32, 3 )
    junk = np.fromfile(file, uint32, 1 )[0]
    junk = np.fromfile(file, float32, 1 )[0]
    junk = file.read(80)
    junk = np.fromfile(file, float32, 4 )
    junk = file.read(80)
    junk = file.read(80)
    junk = np.fromfile(file, int32, 1 )[0]
    junk = file.read(4)
    header_size = np.fromfile(file, uint32, 1)[0]

    wavelength_data_size = np.fromfile(file, uint32, 1 )[0]
    n_wavelengths = np.fromfile(file, uint32, 1 )[0]  #The number of wavelength bins for each spectra
    wavelengths = np.fromfile(file, float32, n_wavelengths ) #The wavelength array in Angstroms
    wavelength_data_size = np.fromfile(file, uint32, 1 )[0]

    if Full:
        sed_grid = np.zeros(( n_wavelengths, n_ages)) #2d Array containing the flux values at all wavelengths (rows) and ages (columns)

        for n in range(n_ages):
            sed_size = np.fromfile(file, uint32, 1 )[0]
            n_fluxes = np.fromfile(file, uint32, 1 )[0] #The number of flux values. This should of course be equal to n_wavelengths
            sed_grid[:,n] = np.fromfile(file, float32, n_fluxes ) #Fluxes in units of Lsun Angstrom^-1. Lsun = 3.826*10**33 ergs-1

            ix = np.fromfile(file, uint32, 1 )[0]  #Not important
            junk = np.fromfile(file, float32, ix ) #I think these might be spectral indicies. Not needed.
            sed_size = np.fromfile(file, uint32, 1 )[0]

        junk_size = np.fromfile(file, uint32, 1)[0]
        junk = np.fromfile(file, uint32, 1)[0]
        junk = np.fromfile(file, float32, junk)
        junk_size = np.fromfile(file, uint32, 1)[0]

        mass_size = np.fromfile(file, uint32, 1)
        n_mass = np.fromfile(file, uint32, 1)[0]
        masses = np.fromfile(file, float32, n_mass) #Mass (Msun)
        mass_size = np.fromfile(file, uint32, 1)[0]

        for n in range(7):
            junk_size = np.fromfile(file, uint32, 1)[0]
            junk = np.fromfile(file, uint32, 1)[0]
            junk = np.fromfile(file, float32, junk)
            junk_size = np.fromfile(file, uint32, 1)[0]

        remnant_size = np.fromfile(file, uint32, 1)
        n_remnant = np.fromfile(file, uint32, 1)[0]
        remnant_masses = np.fromfile(file, float32, n_remnant) #Mass (Msun)
        remnant_size = np.fromfile(file, uint32, 1)[0]

    file.close()
    if Full:
        return ages, wavelengths, sed_grid, masses, remnant_masses
    else:
        return ages, wavelengths

def Read_MN05_Binary(ssp_path,stell_path, logZoZs, big_endian=True, Full = True):
    '''Reads Maraston 2005 SSP data from Bruzual and Charlot 2003 format
    SSP binary files. These files do not include stellar mass as a funciton
    of age for MN05 so this information has to be extracted from elsewhere.
    Returns 1d array of ages (yr),
    1d array of wavelengths (Angstroms), (and if Full = True)
    2d array of sed values (Solar Luminosity per angstrom)
    for different wavelengths (columns) and ages (rows),
    1d array of masses (Msun) (one for each age)'''

    #Correct for problems with reading the binary file. If you get nonsensical results (i.e. really big numbers), change to big_endian = False
    if big_endian:
        uint16, uint32, uint64, float32, float64, int32 = '>u2', '>u4', '>u8', '>f4', '>f8', '>i4'
    else:
        from numpy import uint32, uint16, uint64, float32, float64, int32
    file = open(ssp_path,'rb')

    header_size = np.fromfile(file, uint32, 1)[0]
    n_ages = np.fromfile(file, uint32, 1)[0]  #The number of SSP spectra in the file (one for each age)
    ages = np.fromfile(file, float32, n_ages) #Array of ages (yr)
    #Various bits and pieces of information (on IMF etc) that are not important (hopefully!). This could be cleaned up with a "seek" command.

    junk = np.fromfile(file, uint32, 2 )
    iseg = np.fromfile(file, uint32, 1 )[0]
    junk = np.fromfile(file, float32, 6*iseg )
    junk = np.fromfile(file, float32, 3 )
    junk = np.fromfile(file, uint32, 1 )[0]
    junk = np.fromfile(file, float32, 1 )[0]
    junk = file.read(80)
    junk = np.fromfile(file, float32, 4 )
    junk = file.read(80)
    junk = file.read(80)
    junk = np.fromfile(file, int32, 1 )[0]
    junk = file.read(4)
    header_size = np.fromfile(file, uint32, 1)[0]

    wavelength_data_size = np.fromfile(file, uint32, 1 )[0]
    n_wavelengths = np.fromfile(file, uint32, 1 )[0]  #The number of wavelength bins for each spectra
    wavelengths = np.fromfile(file, float32, n_wavelengths ) #The wavelength array in Angstroms
    wavelength_data_size = np.fromfile(file, uint32, 1 )[0]

    if Full:
        sed_grid = np.zeros(( n_wavelengths, n_ages)) #2d Array containing the flux values at all wavelengths (rows) and ages (columns)

        for n in range(n_ages):
            sed_size = np.fromfile(file, uint32, 1 )[0]
            n_fluxes = np.fromfile(file, uint32, 1 )[0] #The number of flux values. This should of course be equal to n_wavelengths
            sed_grid[:,n] = np.fromfile(file, float32, n_fluxes ) #Fluxes in units of Lsun Angstrom^-1. Lsun = 3.826*10**33 ergs-1
            
            sed_size = np.fromfile(file, uint32, 1 )[0]

    file.close()

    # Read stellar mass as a function of age from a seperate file.
    (logZoZs_ssp, age, masses,malive) = np.loadtxt(stell_path, unpack = True, usecols=(0,1,2,3))
    # Find the closest metallicity to the one asked for.
    met_closest = np.unique(logZoZs_ssp)[np.argmin(abs(logZoZs-np.unique(logZoZs_ssp)))]
    if abs(logZoZs - met_closest) > 0.1:
        print "Warning: log(Z/Zsun)=", logZoZs," was not found for MN05 SPS, using log(Z/Zsun)=",met_closest," instead."
    remnant_masses = masses - malive
    masses = masses[logZoZs_ssp==met_closest]
    remnant_masses = remnant_masses[logZoZs_ssp==met_closest]

    if Full:
        return ages, wavelengths, sed_grid, masses, remnant_masses
    else:
        return ages, wavelengths


def Read_pre_bc99_Binary(file_path, big_endian=True, Full = True):
    '''Reads Bruzual and Charlot 2003 SSP binary files.
    Returns 1d array of ages (yr),
    1d array of wavelengths (Angstroms), (and if Full = True)
    2d array of sed values (Solar Luminosity per angstrom)
    for different wavelengths (columns) and ages (rows),
    1d array of masses (Msun) (one for each age)'''

    #Correct for problems with reading the binary file. If you get nonsensical results (i.e. really big numbers), change to big_endian = False
    if big_endian:
        uint16, uint32, uint64, float32, float64, int32 = '>u2', '>u4', '>u8', '>f4', '>f8', '>i4'
    else:
        from numpy import uint32, uint16, uint64, float32, float64, int32
    file = open(file_path,'rb')

    header_size = np.fromfile(file, uint32, 1)[0]
    n_ages = np.fromfile(file, uint32, 1)[0]  #The number of SSP spectra in the file (one for each age)
    ages = np.fromfile(file, float32, n_ages) #Array of ages (yr)
    #Various bits and pieces of information (on IMF etc) that are not important (hopefully!). This could be cleaned up with a "seek" command.
    iseg = np.fromfile(file, uint32, 1 )[0]
    junk = np.fromfile(file, float32, 6*iseg )
    junk = np.fromfile(file, float32, 3 )
    junk = np.fromfile(file, uint32, 1 )[0]
    junk = np.fromfile(file, float32, 1 )[0]
    junk = file.read(80)
    header_size = np.fromfile(file, uint32, 1)[0]

    wavelength_data_size = np.fromfile(file, uint32, 1 )[0]
    n_wavelengths = np.fromfile(file, uint32, 1 )[0]  #The number of wavelength bins for each spectra
    wavelengths = np.fromfile(file, float32, n_wavelengths ) #The wavelength array in Angstroms
    wavelength_data_size = np.fromfile(file, uint32, 1 )[0]

    if Full:
        sed_grid = np.zeros(( n_wavelengths, n_ages)) #2d Array containing the flux values at all wavelengths (rows) and ages (columns)

        for n in range(n_ages):
            sed_size = np.fromfile(file, uint32, 1 )[0]
            n_fluxes = np.fromfile(file, uint32, 1 )[0] #The number of flux values. This should of course be equal to n_wavelengths
            sed_grid[:,n] = np.fromfile(file, float32, n_fluxes ) #Fluxes in units of Lsun Angstrom^-1. Lsun = 3.826*10**33 ergs-1

            #ix = np.fromfile(file, uint32, 1 )[0]  #Not important
            #junk = np.fromfile(file, float32, ix ) #I think these might be spectral indicies. Not needed.
            sed_size = np.fromfile(file, uint32, 1 )[0]

        junk_size = np.fromfile(file, uint32, 1)[0]
        junk = np.fromfile(file, uint32, 1)[0]
        junk = np.fromfile(file, float32, junk)
        junk_size = np.fromfile(file, uint32, 1)[0]

        mass_size = np.fromfile(file, uint32, 1)
        n_mass = np.fromfile(file, uint32, 1)[0]
        masses = np.fromfile(file, float32, n_mass) #Mass (Msun)
        mass_size = np.fromfile(file, uint32, 1)[0]

        for n in range(7):
            junk_size = np.fromfile(file, uint32, 1)[0]
            junk = np.fromfile(file, uint32, 1)[0]
            junk = np.fromfile(file, float32, junk)
            junk_size = np.fromfile(file, uint32, 1)[0]

        remnant_size = np.fromfile(file, uint32, 1)
        n_remnant = np.fromfile(file, uint32, 1)[0]
        remnant_masses = np.fromfile(file, float32, n_remnant) #Mass (Msun)
        remnant_size = np.fromfile(file, uint32, 1)[0]

    file.close()
    if Full:
        return ages, wavelengths, sed_grid, masses, remnant_masses
    else:
        return ages, wavelengths

#path = '/gpfs/data/Galform/Data/stellar_pop/bc99_ssp_k83_z0200.ised'
#a,b,c,d,e = Read_pre_bc99_Binary(path)
#print e
#exit()

def Read_SSP_Old_Binary(file_path, big_endian=True, Full = True):
    '''Reads SSP binary files stored in the old format.
    Returns 1d array of ages (yr),
    1d array of wavelengths (Angstroms), (and if Full = True)
    2d array of sed values (Solar Luminosity per angstrom)
    for different wavelengths (columns) and ages (rows),
    1d array of masses (Msun) (one for each age)'''

    #Correct for problems with reading the binary file. If you get nonsensical results (i.e. really big numbers), change to big_endian = False
    if big_endian:
        uint16, uint32, uint64, float32, float64, int32 = '>u2', '>u4', '>u8', '>f4', '>f8', '>i4'
    else:
        from numpy import uint32, uint16, uint64, float32, float64, int32

    file = open(file_path,'rb')

    header_size = np.fromfile(file, uint32, 1)[0]
    n_ages = np.fromfile(file, uint32, 1)[0]  #The number of SSP spectra in the file (one for each age)
    ages = np.fromfile(file, float32, n_ages) #Array of ages (yr)

    #Various bits and pieces of information (on IMF etc) that are not important (hopefully!). This could be cleaned up with a "seek" command.
    iseg = np.fromfile(file, uint32, 1 )[0]
    junk = np.fromfile(file, float32, 6*iseg )
    junk = np.fromfile(file, float32, 3 )
    junk = np.fromfile(file, uint32, 1 )[0]
    junk = np.fromfile(file, float32, 1 )[0]
    junk = np.fromfile(file, uint32, 1)[0]
    header_size = np.fromfile(file, uint32, 1)[0]

    wavelength_data_size = np.fromfile(file, uint32, 1 )[0]
    n_wavelengths = np.fromfile(file, uint32, 1 )[0]  #The number of wavelength bins for each spectra
    wavelengths = np.fromfile(file, float32, n_wavelengths ) #The wavelength array in Angstroms
    wavelength_data_size = np.fromfile(file, uint32, 1 )[0]

    if Full:
        sed_grid = np.zeros(( n_wavelengths, n_ages)) #2d Array containing the flux values at all wavelengths (rows) and ages (columns)
        
        for n in range(n_ages):
            sed_size = np.fromfile(file, uint32, 1 )[0]
            n_fluxes = np.fromfile(file, uint32, 1 )[0] #The number of flux values. This should of course be equal to n_wavelengths

            #print np.fromfile(file, float32, n_fluxes )
            #exit()
            sed_grid[:,n] = np.fromfile(file, float32, n_fluxes ) #Fluxes in units of Lsun Angstrom^-1. Lsun = 3.826*10**33 ergs-1

            #print n_fluxes, n_wavelengths, n_ages
            #print np.fromfile(file, float32, n_fluxes )[0]
            #exit()
            
            #ix = np.fromfile(file, uint32, 1 )[0]  #Not important
            #junk = np.fromfile(file, float32, ix ) #I think these might be spectral indicies. Not needed.
            sed_size = np.fromfile(file, uint32, 1 )[0]

        junk_size = np.fromfile(file, uint32, 1)[0]
        junk = np.fromfile(file, uint32, 1)[0]
        junk = np.fromfile(file, float32, junk)
        junk_size = np.fromfile(file, uint32, 1)[0]

        mass_size = np.fromfile(file, uint32, 1)
        n_mass = np.fromfile(file, uint32, 1)[0]
        masses = np.fromfile(file, float32, n_mass) #Mass (Msun)
        mass_size = np.fromfile(file, uint32, 1)[0]

        for n in range(7):
            junk_size = np.fromfile(file, uint32, 1)[0]
            junk = np.fromfile(file, uint32, 1)[0]
            junk = np.fromfile(file, float32, junk)
            junk_size = np.fromfile(file, uint32, 1)[0]

        remnant_size = np.fromfile(file, uint32, 1)
        n_remnant = np.fromfile(file, uint32, 1)[0]
        remnant_masses = np.fromfile(file, float32, n_remnant) #Mass (Msun)
        remnant_size = np.fromfile(file, uint32, 1)[0]

    file.close()
    if Full:
        return ages, wavelengths, sed_grid, masses, remnant_masses
    else:
        return ages, wavelengths


def Filter_Response(file_path,id_band, sed_wavelengths, z):
    '''Calculate the filter response interpolated
    onto the grid of wavelengths for the template sed.
    Returns a list of response arrays of filters
    corresponding to the id_band sequence'''
    filter_wavelengths, response, n_points = Read_Filter_File(file_path)

    filter_wavelengths_galaxy_rest_frame = filter_wavelengths / (1.0+z)

    filter_wavelength_list = np.split(filter_wavelengths_galaxy_rest_frame,np.cumsum(n_points))
    response_list = np.split(response,np.cumsum(n_points))

    interpolated_response_grid = np.zeros((len(id_band),len(sed_wavelengths)))
    
    for n in range(len(id_band)):

        interpolated_response_grid[n] += Linear_Interpolation(filter_wavelength_list[id_band[n]-1], response_list[id_band[n]-1], sed_wavelengths,bounds_error = False, fill_value =0.0 )
    
    return interpolated_response_grid

def Composite_Spectra(age_grid, wavelengths, sed_grid,mass,remnant_mass,tau, galaxy_age, SFH_Type, Recyled=False):
    '''Buid composite spectrum with exponentially
    declining SFH from a BCO3 sed file.
    Sed grid axes should be 1)wavelength 2) age
    Tau is star_formation e-folding time_scale (Gyr)
    Galaxy age should be in units of (yr)
    Recycled = True also returns the recycled fraction.'''

    Gyr = 10.0**9.0            #Convert to years

    if SFH_Type == 'ettau' and tau != 0.0:
        #normalisation = 1.0 / (tau*Gyr * ( 1.0 - np.exp(-(galaxy_age/(tau*Gyr)))) ) #This is 0-galaxy age normalisation
        normalisation = 1.0 / (tau*Gyr) #This is 0-infinity normalisation
    elif SFH_Type == 'tettau':
        normalisation = 1.0/ (tau*Gyr)**2 #This is 0-infinity normalisation
    elif SFH_Type == 'csf' or tau==0.0:
        normalisation = 1.0/ galaxy_age # This is 0-age normalisation
    else:
        print 'Error: SFH_Type = ',SFH_Type,' is not valid. Please see Fit_Parameters.py'
        exit()

    age_steps = age_grid[age_grid <= galaxy_age]
    n_steps = len(age_steps)

    sfr_reverse = np.zeros(int(n_steps))  #This is a convolution integral so sfr is evaluated at T-t when sed is evaluated at t.
    for n in range(int(n_steps)):
        if SFH_Type == 'ettau' and tau != 0.0:
            sfr_reverse[n] = normalisation * np.exp(-(galaxy_age-age_steps[n])/(tau*Gyr))
        elif SFH_Type == 'tettau':
            sfr_reverse[n] = normalisation * galaxy_age * np.exp(-(galaxy_age-age_steps[n])/(tau*Gyr))
        elif SFH_Type == 'csf' or tau==0.0:
            sfr_reverse[n] = normalisation

    fi = np.zeros(len(wavelengths))
    composite_mass = 0
    composite_remnant_mass = 0
    if Recyled:
        recyled_mass = 0

    mass_weighted_age = 0
    mass_ira = 0 # Mass to be used if instantaneous recycling approximation used in fitting.

    # Reshape arrays because of annoying numpy broadcasting rules.
    a_s_col = np.reshape(age_steps,(n_steps,1))
    sfr_r_col = np.reshape(sfr_reverse,(n_steps,1))
    m_col = np.reshape(mass[0:n_steps],(n_steps,1))
    rem_col = np.reshape(remnant_mass[0:n_steps],(n_steps,1))
    sed_swap = np.swapaxes(sed_grid,0,1)
    
    fi = np.sum(0.5 * (a_s_col[1:]-a_s_col[0:n_steps-1]) * (sfr_r_col[1:]*sed_swap[1:n_steps] + sfr_r_col[0:n_steps-1]*sed_swap[0:n_steps-1]),axis=0)
    composite_mass = np.sum(0.5 * (a_s_col[1:]-a_s_col[0:n_steps-1]) * (sfr_r_col[1:]*m_col[1:] + sfr_r_col[0:n_steps-1]*m_col[0:n_steps-1]),axis=0)
    composite_remnant_mass = np.sum(0.5 * (a_s_col[1:]-a_s_col[0:n_steps-1]) * (sfr_r_col[1:]*rem_col[1:] + sfr_r_col[0:n_steps-1]*rem_col[0:n_steps-1]),axis=0)
    if Recyled:
        recyled_mass = np.sum(0.5 * (a_s_col[1:]-a_s_col[0:n_steps-1]) * (sfr_r_col[1:]*(1-rem_col[1:]-m_col[1:]) + sfr_r_col[0:n_steps-1]*(1-m_col[0:n_steps-1]-rem_col[0:n_steps-1]) ), axis=0)
    mass_weighted_age = np.sum(0.5*((a_s_col[1:]+a_s_col[0:n_steps-1])/Gyr) * 0.5 * (a_s_col[1:]-a_s_col[0:n_steps-1]) * (sfr_r_col[1:]*m_col[1:] + sfr_r_col[0:n_steps-1]*m_col[0:n_steps-1]),axis=0)
    mass_ira = np.sum(0.5*(a_s_col[1:]-a_s_col[0:n_steps-1])*(sfr_r_col[1:]+sfr_r_col[0:n_steps-1]))
    
    mass_weighted_age *= 1/composite_mass
    if Recyled:
        return fi, composite_mass, composite_remnant_mass, mass_weighted_age, mass_ira, recyled_mass
    else:
        return fi, composite_mass, composite_remnant_mass, mass_weighted_age, mass_ira

def Build_Age_Grid(file_path,tau_grid, galaxy_age_grid, extra_time_grid, SFH_Type,SPS,mn05_stell=None, Zstar=None):
    '''Calls Composite_Spectra for a grid of Tau values
    and ages limited by age min/max'''

    if SPS[0:4] == 'BC03':
        age_grid, wavelengths, sed_grid, mass, remnant_mass = Read_BC03_Binary(file_path)
    elif SPS[0:4] == 'mn05':
        if mn05_stell is None or Zstar is None:
            print "Error: For Maraston 05 SPS you need to give the path of the stellar mass file and the corresponding metallicity"
            quit()
        age_grid, wavelengths, sed_grid, mass,remnant_mass = Read_MN05_Binary(file_path,mn05_stell,np.log10(Zstar/0.02))
    else:
        print "Error, SPS=",SPS," not found."
        exit()

    #To improve SFH integration accuracy, extra time steps are added via linear interpolation
    extra_sed_grid = Linear_Interpolation(age_grid,np.swapaxes(sed_grid,0,1),extra_time_grid)
    extra_mass_grid = Linear_Interpolation(age_grid,mass,extra_time_grid)[np.argsort(extra_time_grid)]
    extra_rem_grid = Linear_Interpolation(age_grid,remnant_mass,extra_time_grid)[np.argsort(extra_time_grid)]

    new_age_grid = np.append(age_grid,extra_time_grid)
    new_sed_grid = np.append(np.swapaxes(sed_grid,0,1),extra_sed_grid,axis=0)[np.argsort(new_age_grid)]
    new_mass = np.append(mass,extra_mass_grid)[np.argsort(new_age_grid)]
    new_remnant_mass = np.append(remnant_mass,extra_rem_grid)[np.argsort(new_age_grid)]

    new_age_grid = np.sort(new_age_grid)
    new_sed_grid = np.swapaxes(new_sed_grid,0,1)

    #Flux grid in Lsun per Angstrom. 1) Wavelength 2) Galaxy Age 3) Tau
    fi_grid = np.zeros((len(wavelengths),len(galaxy_age_grid),len(tau_grid)))
    
    #Mass_grid (Msun). The mass in stars of the composite stellar population at age t. 1)Galaxy Age 2) Tau
    mass_grid = np.zeros((len(galaxy_age_grid),len(tau_grid)))
    
    #Remnant_grid (Msun). The mass in stellar remnants of the composite population at age t. 1)Galaxy Age 2) Tau
    remnant_mass_grid = np.zeros((len(galaxy_age_grid),len(tau_grid)))

    #Mass weighted age grid (Gyr)
    mass_weighted_age_grid = np.zeros((len(galaxy_age_grid),len(tau_grid)))

    #Mass_IRA grid (Msun). The total stellar mass if the instanteous recycling approximation is made in the fitting.
    mass_ira_grid = np.zeros_like(mass_grid)

    #Loop to get fluxes
    for n in range(len(galaxy_age_grid)):
        for m in range(len(tau_grid)):
            fi_grid[:,n,m], mass_grid[n,m], remnant_mass_grid[n,m], mass_weighted_age_grid[n,m], mass_ira_grid[n,m] = Composite_Spectra(new_age_grid, wavelengths, new_sed_grid, new_mass, new_remnant_mass, tau_grid[m], galaxy_age_grid[n], SFH_Type)
    
    return fi_grid, mass_grid, remnant_mass_grid, mass_weighted_age_grid, mass_ira_grid

def IGM_Attenuation(wavelengths_rest,zem):
    '''Apply IGM absorption of rest frame uv light
    via Lyman series photo-excitation and continuum
    photo-ionisation by intervening HI clouds.
    Follows the prescription of Madau et al. 1995'''

    wH = 912.0 #Angstroms. This is the maximum wavelength for photo ionisation
    
    lambda_obs_over_wH = wavelengths_rest*(1.0+zem)/wH # Observed wavelength divided by Lyman-continuum wavelength for hydrogren 

    dt1=0.00360*(lambda_obs_over_wH*(1.0-1.0/( 2.0**2)))**3.46 #Effective optical depth of the Lyman series transitions
    dt2=0.00170*(lambda_obs_over_wH*(1.0-1.0/( 3.0**2)))**3.46
    dt3=0.00120*(lambda_obs_over_wH*(1.0-1.0/( 4.0**2)))**3.46
    dt4=0.00093*(lambda_obs_over_wH*(1.0-1.0/( 5.0**2)))**3.46
    dt5=0.00093*(lambda_obs_over_wH*(1.0-1.0/( 6.0**2)))**3.46
    dt6=0.00093*(lambda_obs_over_wH*(1.0-1.0/( 7.0**2)))**3.46
    dt7=0.00093*(lambda_obs_over_wH*(1.0-1.0/( 8.0**2)))**3.46
    dt8=0.00093*(lambda_obs_over_wH*(1.0-1.0/( 9.0**2)))**3.46
    dt9=0.00093*(lambda_obs_over_wH*(1.0-1.0/(10.0**2)))**3.46

    xem=1.0+zem
    xc=lambda_obs_over_wH
    #Continuum effective optical depth
    tc=0.25*(xc**3)*(xem**0.46-xc**0.46)+9.4*(xc**1.5)*(xem**0.18-xc**0.18)-0.7*(xc**3)*(1.0/xc**1.32-1.0/xem **1.32)-0.023*(xem **1.68-xc**1.68)

    t_tot=np.zeros(len(wavelengths_rest))  #Net optical depth

    for n in range(len(t_tot)):
        if lambda_obs_over_wH[n] < (1.0+zem)/(1.0-1.0/( 2.0**2)):
            t_tot[n] += dt1[n]
        if lambda_obs_over_wH[n] < (1.0+zem)/(1.0-1.0/( 3.0**2)):
            t_tot[n] += dt2[n]
        if lambda_obs_over_wH[n] < (1.0+zem)/(1.0-1.0/( 4.0**2)):
            t_tot[n] += dt3[n]
        if lambda_obs_over_wH[n] < (1.0+zem)/(1.0-1.0/( 5.0**2)):
            t_tot[n] += dt4[n]
        if lambda_obs_over_wH[n] < (1.0+zem)/(1.0-1.0/( 6.0**2)):
            t_tot[n] += dt5[n]
        if lambda_obs_over_wH[n] < (1.0+zem)/(1.0-1.0/( 7.0**2)):
            t_tot[n] += dt6[n]
        if lambda_obs_over_wH[n] < (1.0+zem)/(1.0-1.0/( 8.0**2)):
            t_tot[n] += dt7[n]
        if lambda_obs_over_wH[n] < (1.0+zem)/(1.0-1.0/( 9.0**2)):
            t_tot[n] += dt8[n]
        if lambda_obs_over_wH[n] < (1.0+zem)/(1.0-1.0/(10.0**2)):
            t_tot[n] += dt9[n]

        if lambda_obs_over_wH[n] < (1.0+zem):
           t_tot[n] += tc[n]
    
    IGM_Attenuation_Madau = np.exp(-t_tot) 
    return IGM_Attenuation_Madau

def Calzetti_Attenuation(wavelengths,EBV,EBV_Grid = True):
    '''applies a Calzetti (2000) attenuation with an extension into the UV
    from Leitherer et al. (2002) to derive attenuation factors as a function
    of wavelength (Angstroms). Attenuation is characterised by colour excess
    E(B-V). Return attenuatuation factors within wavelength range valid for
    the Calzetti law. Values outside this range are simply set to 1.'''
    
    k_long = lambda x: 2.659*(-1.857+1.04/x) + 4.05
    k_short = lambda x: 2.659*(-2.156+1.509/x -0.198/(x**2) +0.011/(x**3)) + 4.05
    #From Leitherer 2003
    k_UV = lambda x: 5.472 + 0.671/x -9.218*10**-3 / x**2 + 2.620*10**-3 / x**3

    if EBV_Grid:
        atten_factor = np.zeros((len(EBV),len(wavelengths)))

    else:
        atten_factor = np.zeros(len(wavelengths))
        
    for n in range(len(wavelengths)):
        if wavelengths[n] > 0.63*10**4.0 and wavelengths[n] <= 2.2*10**4.0:
            if EBV_Grid:
                atten_factor[:,n] += 10**(-0.4*EBV*k_long(wavelengths[n]*10**-4.0))
            else:
                atten_factor[n] += 10**(-0.4*EBV*k_long(wavelengths[n]*10**-4.0))

        elif wavelengths[n] <= 0.63*10**4.0 and wavelengths[n] >= 0.12*10**4.0:
            if EBV_Grid:
                atten_factor[:,n] += 10**(-0.4*EBV*k_short(wavelengths[n]*10**-4.0))
            else:
                atten_factor[n] += 10**(-0.4*EBV*k_short(wavelengths[n]*10**-4.0))

        elif wavelengths[n] < 0.12*10**4.0 and wavelengths[n] >= 0.097*10**4.0:
            if EBV_Grid:
                atten_factor[:,n] += 10**(-0.4*EBV*k_UV(wavelengths[n]*10**-4.0))
            else:
                atten_factor[n] += 10**(-0.4*EBV*k_UV(wavelengths[n]*10**-4.0))
                
        else:
            if EBV_Grid:
                atten_factor[:,n] += 1.0
            else:
                atten_factor[n] += 1.0
                
    return atten_factor

def Calculate_AB_Magnitudes(flux_grid, wavelength, response, print_status=True):
    '''Calculate absolute AB magnitudes for the template spectra.
    Returned grid has axes: 1)Band 2)Attenuation 3) Metallicity
    4) Age 5) Tau'''
    # 1)Band 2)Attenuation 3) Metallicity 4) Age 5) Tau
    top = np.zeros(( len(response[:,0]), len(flux_grid[0,:,0,0,0]), len(flux_grid[0,0,:,0,0]), len(flux_grid[0,0,0,:,0]), len(flux_grid[0,0,0,0,:]) ))
    bottom = np.zeros(( len(response[:,0]), len(flux_grid[0,:,0,0,0]), len(flux_grid[0,0,:,0,0]), len(flux_grid[0,0,0,:,0]), len(flux_grid[0,0,0,0,:]) ))

    n_w = len(wavelength)

    #Perform Trapezium rule integration
    if print_status:
        print 'calculating magnitudes for bands:'
    for m in range(len(response[:,0])):
        if print_status:
            print m, 'of', len(response[:,0])

        n_w = len(wavelength)
        w_col = np.reshape(wavelength,(n_w,1,1,1,1))       
        re_col = np.reshape(response[m],(n_w,1,1,1,1))

        top[m] = np.sum(0.5 * (w_col[1:] - w_col[0:n_w-1]) * (flux_grid[1:] * re_col[1:] + flux_grid[0:n_w-1] * re_col[0:n_w-1]),axis=0)
        bottom[m] = np.sum(0.10893 * 0.5 * (w_col[1:] - w_col[0:n_w-1]) * ( re_col[1:] / w_col[1:]**2 + re_col[0:n_w-1] / w_col[0:n_w-1]**2 ),axis=0)

    # 1)Band 2)Attenuation 3) Metallicity 4) Age 5) Tau
    mag = -2.5*np.log10(top/bottom)
    return mag

def Print_Statements(DustFit,DustGalform,Calzetti,One_Metallicity,Metallicity_ind,One_Tau,Tau_ind,Santini_Limit,Extra_Metallicity,ForceCorrectZ,Instantaneous_Recycling,template_Z_grid,tau_grid,SFH_Type, Rest_Frame, Weighted_Average,Dust_Law,UseCalzetti):
    if One_Metallicity and ForceCorrectZ:
        print 'Error: It does not make sense to use One_Metallicity and ForceCorrectZ at the same time!'
        quit()
    if DustGalform and UseCalzetti:
        print 'Error: The sample gals attenuation and the Calzetti correction should not be used at the same time!'
        quit()
    if not DustFit and not DustGalform:
        print 'All dust effects are ignored in GALFORM and the fitting'
    elif not DustGalform:
        print 'Dust included in fit but not in GALFORM'
    elif not DustFit:
        print 'E(B-V) = 0 in fit but dust applied in GALFORM'

    if Dust_Law == 'SlabCalzetti':
        print 'Slab model is being used for template galaxies with a Calzetti extionction curve'
    if Calzetti:
        print 'Calzetti law has been applied to Galform galaxies instead of standard calcualtion performed by sample_gals'

    if One_Metallicity:
        print 'Fitting is performed at fixed metallicity = ',template_Z_grid[Metallicity_ind]

    if One_Tau:
        print 'Fitting is performed at fixed Tau = ', tau_grid[Tau_ind]

    if Santini_Limit:
        print 'Following Santini12 and 09: Supersolar metallicities at z > 1.0 are excluded from fitting'

    if Extra_Metallicity:
        print 'Extra metallicities calculated via interpolation are included in the fitting'

    if ForceCorrectZ:
        print 'Fitting is forced to pick the closest metallicity to the Galform value'

    if Instantaneous_Recycling:
        print 'Also outputting the stellar mass if the Instantaneous Recycling Approximation is made in the fitting in order to be consistent with Galform. This in NOT the choice made by observers'

    if SFH_Type == 'ettau':
        print 'Using exponentially declining SFH'
    elif SFH_Type == 'tettau':
        print 'Using t exp(-t/tau) SFH - age=age of universe'
    else:
        print 'Error: SFH_Type = ',SFH_Type,' is not valid. See Fit_Parameters.py'
        exit()

    if Rest_Frame:
        print 'Filters are not shifted into the rest frame so the same parts of the galaxy SEDs are sampled at all redshifts'

    if Weighted_Average:
        print 'Also outputing the mass you get from performing a exp(-x^2) integral over the parameter space'

def LBG_Selection_Function(z,mag_5sig,apparent_mag,lambda_eff,verbose=False):
    '''Stark et al. (2009) criteria for Lyman Break Galaxy selection 
    at z = 4, 5 or 6.
    Inputs:   z = input redshift.
       mag_5sig = apparent magnitude at 5 sigma depth in each band
       apparent_mag = model galaxy magnitudes (row=band,col=galaxy)
       lambda_eff = effective wavelength of each filter used in apparent mag array
    Outputs: lbg_detected = boolean array of true for each galaxy if detected'''
    
    # Check that appropriate filters have been provided
    B_ind = np.argmin(abs(lambda_eff-4298.0))
    V_ind = np.argmin(abs(lambda_eff-5850.0))
    i_ind = np.argmin(abs(lambda_eff-7658.0))
    z_ind = np.argmin(abs(lambda_eff-9117.0))

    if abs(lambda_eff[B_ind] - 4298.0) > 300.0:
        print "Error: You need to provide a B-band filter to do LBG selection"
        quit()
    if abs(lambda_eff[V_ind] - 5850.0) > 300.0:
        print "Error: You need to provide a V-band filter to do LBG selection"
        quit()
    if abs(lambda_eff[i_ind] - 7658.0) > 300.0:
        print "Error: You need to provide a i-band filter to do LBG selection"
        quit()
    if abs(lambda_eff[z_ind] - 9117.0) > 300.0:
        print "Error: You need to provide a z-band filter to do LBG selection"
        quit()

    # Check the input redshift is sensible
    if z < 4.0 or z>=7.0:
        print "Error, Lyman Break Selection can only be used for 4 <= z < 7"
        quit()
    
    # LBG selection for B-band dropouts at z ~ 4
    if z >= 4.0 and z < 5.0:
        if verbose:
            print "Using z = 4, B-band dropout LBG selection from Stark et al. (2009)"
        lbg_detected = apparent_mag[B_ind] - apparent_mag[V_ind] > 1.1 + apparent_mag[V_ind] - apparent_mag[z_ind]
        lbg_detected[apparent_mag[V_ind]-apparent_mag[z_ind] >= 1.6] = False
        lbg_detected[apparent_mag[B_ind]-apparent_mag[V_ind] <= 1.1] = False
        lbg_detected[apparent_mag[V_ind] >= mag_5sig[V_ind]] = False
        lbg_detected[apparent_mag[i_ind] >= mag_5sig[i_ind]-2.5*np.log10(0.6)] = False

    if z >= 5.0 and z < 6.0:
        if verbose:
            print "Using z = 5, V-band dropout LBG selection from Stark et al. (2009)"
        lbg_detected = (apparent_mag[B_ind] > mag_5sig[B_ind]-2.5*np.log10(0.4)) | (apparent_mag[B_ind]-apparent_mag[i_ind] > apparent_mag[V_ind] - apparent_mag[i_ind] + 1.0)
        lbg_detected[apparent_mag[V_ind]-apparent_mag[i_ind] <= 1.2] = False    
       
        #temp hack from Stark 09 criteria - this doesn't seem to do anything but it makes me feel better - just requires that you have at least 1sigma in i.
        lbg_detected[apparent_mag[i_ind] >= mag_5sig[i_ind] - 2.5*np.log10(0.2)] = False
        #end temp hack

        lbg_detected[(apparent_mag[V_ind]-apparent_mag[i_ind] <= 1.47 + 0.89 * (apparent_mag[i_ind]-apparent_mag[z_ind])) & (apparent_mag[V_ind]-apparent_mag[i_ind] <= 2.0)]= False
        lbg_detected[apparent_mag[i_ind] - apparent_mag[z_ind] >= 1.3] = False
        lbg_detected[apparent_mag[z_ind] >= mag_5sig[z_ind]] = False

    if z >= 6.0 and z<7.0:
        if verbose:
            print "Using z=6, i-band dropout LBG selection from Stark et al. (2009)"
        lbg_detected = (apparent_mag[V_ind] > mag_5sig[V_ind]-2.5*np.log10(0.4)) | (apparent_mag[V_ind]-apparent_mag[z_ind] > 2.8)
        lbg_detected[apparent_mag[i_ind]-apparent_mag[z_ind] <= 1.3] = False
        lbg_detected[apparent_mag[z_ind] >= mag_5sig[z_ind]] = False

    if verbose:
        print len(apparent_mag[0][lbg_detected])," of ",len(lbg_detected), " were selected using LBG selection."

    return lbg_detected
