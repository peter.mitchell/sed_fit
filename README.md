Code to perform broadband spectral energy distribution (SED) fitting of simulated galaxies produced by the semi-analytic galaxy formation model, GALFORM. SED fitting of the type implemented here is a widely used technique in observational astronomy, used to infer principally the stellar mass of galaxies based on their observed brightness at different wavelengths.

The code in this repository was used as part of the following scientific publications, and is described principally in the first link:
https://arxiv.org/pdf/1303.7228.pdf,
https://arxiv.org/pdf/1509.08473.pdf,
https://arxiv.org/pdf/1412.3804.pdf,
https://arxiv.org/pdf/1501.03672.pdf

Various "fit_parameters.py" template files are provided to serve as examples for running SED fitting in different configurations.
