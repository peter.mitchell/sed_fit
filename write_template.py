import numpy as np
import utilities_sed
from fit_parameters_default import *
#print "Using Taylor 11 parameters"
#from fit_parameters_taylor11 import *
#print "Using Vypers parameters"
#from fit_parameters_vypers import *
#print "Using SDSS parameters"
#from fit_parameters_sdss import *
#print "using will's requested parameters"
#from fit_parameters_will import *

#Constants
Gyr = 10.0**9.0
Lsun = 3.826*10**33
parsec = 3.086*10**18

#Read in basic template sed parameters and calculate the galaxy age grid. Write this to file for future reference
if SPS[0:4] == 'BC03':
    age_grid, wavelengths_rest = utilities_sed.Read_BC03_Binary('/gpfs/data/Galform/Data/stellar_pop/'+SPS+'_Z0500.ised',True,False)
elif SPS[0:4] == 'mn05':
    age_grid,wavelengths_rest = utilities_sed.Read_MN05_Binary('/gpfs/data/Galform/Data/stellar_pop/'+SPS+'_hbRed_Z0100.ised',stellarmassfile,np.log10(template_Z_grid[0]/0.02),True,False)
else:
    print "Error, ", SPS, " not recognised."

#Get the dust attenuation factors grid 1) E(B-V) 2) Wavelength
attenuation_factors_dust = utilities_sed.Calzetti_Attenuation(wavelengths_rest,EBV_grid)

if Dust_Law == "SlabCalzetti":
    tau_calzetti = -np.log(attenuation_factors_dust)
    attenuation_factors_dust = (1.0-np.exp(-tau_calzetti))/tau_calzetti
    #Deal with the lower limit. Derived from L'Hopitals rule
    attenuation_factors_dust[tau_calzetti==0] = np.ones(tau_calzetti[tau_calzetti==0].shape)
elif Dust_Law != "Calzetti":
    print "Error: Dust_Law from Fit_Parameters.py not recognised!"

#Redshift Loop
for i in range(len(z_array)):

    z = z_array[i]
    if z == int(z):
        z = int(z)

    SPS_Save = SPS
    if Rest_Frame:
        SPS_Save += '/RestFrame'
    if Dust_Law == "SlabCalzetti":
        SPS_Save += '/SlabCalzetti'
    
    age_max = age_max_array[i]
    
    print 'Building grid for redshift z=',z

    galaxy_age_grid = age_grid[ (age_grid >= age_min*Gyr) & (age_grid <= age_max*Gyr)]
    extra_time_grid = np.arange(extra_t_stepsize,max(galaxy_age_grid),extra_t_stepsize)

    #Galaxy age always set to age of universe - 0.5 Gyr for texp(-t/tau) SFH
    if SFH_Type == 'tettau':
        select = np.arange(0,len(galaxy_age_grid))
        select = select[select!= np.argmin(abs(galaxy_age_grid-(age_max-0.5)*Gyr))]
        galaxy_age_grid = np.delete(galaxy_age_grid,select)

    np.save('/gpfs/data/d72fqv/SED_Fitting/Template_Data/'+SFH_Type+'/'+SPS_Save+'/z'+str(z)+'_age_grid',galaxy_age_grid/Gyr)

    #Get the IGM attenuation factors
    attenuation_factors_igm = utilities_sed.IGM_Attenuation(wavelengths_rest, z)

    #Get filter response grid. Filter responses have been been shifted into the galaxy rest frame and interpolated onto the sed wavelength grid. 1) id_band 2)wavelength
    response_grid = utilities_sed.Filter_Response(filter_path, id_band, wavelengths_rest, z)

    #If set in Fit_Parameters.py, prevent the shift into the observer frame
    if Rest_Frame:
        response_grid = utilities_sed.Filter_Response(filter_path, id_band, wavelengths_rest, 0.0)

    n_Z_sps = len(sps_files)                    #Number of metallicities provided by sps files
    n_Z_tot = len(template_Z_grid)    #Total number of metallicities to be calculated for template grid

    #The master flux grid (Lsun per angstrom) with axes of 1)attenuation 2)metallicity 3)wavelength 4)age 5)tau
    flux_grid = np.zeros((len(EBV_grid),n_Z_tot,len(wavelengths_rest),len(galaxy_age_grid),len(tau_grid)))
    #The master stellar mass (Msun) grid with 1)metallicity 2) age 3) tau
    mass_grid = np.zeros((n_Z_tot,len(galaxy_age_grid),len(tau_grid)))
    #The master stellar remnant mass (Msun) grid with 1)metallicity 2) age 3) tau
    mass_rem_grid = np.zeros((n_Z_tot,len(galaxy_age_grid),len(tau_grid)))
    #Mass weighted age grid (Gyr)
    mass_weighted_age_grid = np.zeros((n_Z_tot,len(galaxy_age_grid),len(tau_grid)))
    #Total stellar mass grid if Instanteous recycling is wanted in fitting
    mass_ira_grid = np.zeros_like(mass_grid)

    print 'Building the parameter grid:'
    for n in range(n_Z_sps):
        flux_grid[:,n], mass_grid[n], mass_rem_grid[n], mass_weighted_age_grid[n], mass_ira_grid[n] = utilities_sed.Build_Age_Grid(sps_dir+sps_files[n], tau_grid, galaxy_age_grid, extra_time_grid, SFH_Type,SPS,stellarmassfile,template_Z_grid[n])
        print n+1 , ' of ' , n_Z_sps, ' complete'

    #Calculate seds via linear interpolation for extra metallicities if added in Fit_Parameters.py.
    if n_Z_sps != n_Z_tot:
        print 'Add extra metallicities'
        flux_grid =  np.swapaxes(flux_grid,0,1)
        flux_grid[n_Z_sps:n_Z_tot] = utilities_sed.Linear_Interpolation(np.log10(template_Z_grid[0:n_Z_sps]),flux_grid[0:n_Z_sps],np.log10(template_Z_grid[n_Z_sps:n_Z_tot]),bounds_error=False,fill_value=0.0)
        flux_grid =  np.swapaxes(flux_grid,0,1)

        mass_grid[n_Z_sps:n_Z_tot] = utilities_sed.Linear_Interpolation(np.log10(template_Z_grid[0:n_Z_sps]),mass_grid[0:n_Z_sps],np.log10(template_Z_grid[n_Z_sps:n_Z_tot]),bounds_error=False,fill_value=0.0)
        mass_rem_grid[n_Z_sps:n_Z_tot] = utilities_sed.Linear_Interpolation(np.log10(template_Z_grid[0:n_Z_sps]),mass_rem_grid[0:n_Z_sps],np.log10(template_Z_grid[n_Z_sps:n_Z_tot]),bounds_error=False,fill_value=0.0)
        mass_weighted_age_grid[n_Z_sps:n_Z_tot] = utilities_sed.Linear_Interpolation(np.log10(template_Z_grid[0:n_Z_sps]),mass_weighted_age_grid[0:n_Z_sps],np.log10(template_Z_grid[n_Z_sps:n_Z_tot]),bounds_error=False,fill_value=0.0)
        mass_ira_grid[n_Z_sps:n_Z_tot] = utilities_sed.Linear_Interpolation(np.log10(template_Z_grid[0:n_Z_sps]),mass_ira_grid[0:n_Z_sps],np.log10(template_Z_grid[n_Z_sps:n_Z_tot]),bounds_error=False,fill_value=0.0)
        print 'Done'

    #Complete flux grid by applying the dust attenuation factors
    flux_grid = np.swapaxes(flux_grid,1,2) #1) attenuation 2) wavelength 3) metallicity 4) age 5) tau

    for n in range(len(EBV_grid)):
        for m in range(len(wavelengths_rest)):
            flux_grid[n,m] *= attenuation_factors_dust[n,m]

    #Swap flux grid axes such that 1)wavelength 2)attenuation 3)metallicity 4)age 5) tau
    flux_grid = np.swapaxes(flux_grid,1,0)

    #Apply IGM Absorption via the Madau 1995 prescription
    for n in range(len(wavelengths_rest)):
        flux_grid[n] *= attenuation_factors_igm[n]

    #Compute AB absoulte magnitudes in bands specified by id_band. This involves integrating over the response functions from the filters file

    #Convert flux to erg s^-1 cm^-2 A^-1
    flux_grid *= Lsun / (4*np.pi*(10*parsec)**2)

    #AB absolute magnitude grid # 1)Band 2)Attenuation 3) Metallicity 4) Age 5) Tau
    magAB_grid = utilities_sed.Calculate_AB_Magnitudes(flux_grid, wavelengths_rest, response_grid)

    #Save arrays to files
    np.save('/gpfs/data/d72fqv/SED_Fitting/Template_Data/'+SFH_Type+'/'+SPS_Save+'/z'+str(z)+'_magAB_grid',magAB_grid)
    np.save('/gpfs/data/d72fqv/SED_Fitting/Template_Data/'+SFH_Type+'/'+SPS_Save+'/z'+str(z)+'_stellar_mass_grid',mass_grid)
    np.save('/gpfs/data/d72fqv/SED_Fitting/Template_Data/'+SFH_Type+'/'+SPS_Save+'/z'+str(z)+'_remnant_mass_grid',mass_rem_grid)
    np.save('/gpfs/data/d72fqv/SED_Fitting/Template_Data/'+SFH_Type+'/'+SPS_Save+'/z'+str(z)+'_mass_weighted_age_grid',mass_weighted_age_grid)
    np.save('/gpfs/data/d72fqv/SED_Fitting/Template_Data/'+SFH_Type+'/'+SPS_Save+'/z'+str(z)+'_mass_ira_grid',mass_ira_grid)
