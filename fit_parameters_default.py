#File defining the parameter space that the SED fitting will search over along with various other options
#All of the variables/arrays etc defined in this file are used in Write_Template.py and SED_Fit.py

import numpy as np
from scipy import integrate


################################  Redshifts and cosmology #################################################

# Note some of the standard Mill1 output redshifts are:
# z_list = [6.197,4.888,4.179,3.87,3.576,3.06,2.422,2.07,1.504,0.989,0.755,0.509,0.242,0.089,0.0]

#z_list = [0]

# Grid for Cedric's model 25/4/14
#z_list = [4.18,3.06,2.07,0.99,0.51,0]

# Grid for Will
z_list = [6.197, 4.179, 2.07, 0]

z_array = np.array(z_list)

# Assumed cosmological paramters used to calculate the age of the universe at each redshift. The SED fitting is NOT particularly sensitive to this!
h0 = 0.7 ; omega0 = 0.27 ; lambda0 = 0.73

H = lambda z: 100 * h0 * (omega0*(1.0+z)**3.0 + lambda0)**0.5
age_int = lambda z: 1.0/((1+z)*H(z))

# The age of the universe at this redshift. This is only used to set the
# maximum age that the stellar population of a galaxy is allowed to have.

age_universe = np.zeros(len(z_array))
for n in range(len(z_array)):
    age_universe[n] = integrate.quad(age_int,float(z_array[n]),np.inf)[0] * 979.16





############################ Stellar Population Synthesis Files and metallicity grid #########################


# Desired SPS model and IMF
# Bruzual and Charlot (2003), Padova 94 tracks, Salpeter IMF - SPS ==> 'BC03_pd94_lr_sal'
# Bruzual and Charlot (2003), Padova 94 tracks, Chabrier IMF - SPS ==> 'BC03_pd94_lr_cha'
# Maraston et al. (2005), Kroupa IMF ==> SPS = 'mn05_ssp_krp'


# Set the metallicity grid and corresponding file names. The file names will be different for different SPS models.

sps_dir = '/gpfs/data/Galform/Data/stellar_pop/'

print "Using B&C 2003 SPS"
SPS = "BC03_pd94_lr_cha"
sps_files = (SPS+'_Z0500.ised',SPS+'_Z0200.ised',SPS+'_Z0080.ised',SPS+'_Z0040.ised',SPS+'_Z0004.ised') # Bruzual & Charlot 2003

#print "Using Maraston 2005 SPS with kroupa IMF"
#SPS = "mn05_ssp_krp"
#sps_files = (SPS+'_hbRed_Z0400.ised',SPS+'_hbRed_Z0200.ised',SPS+'_hbRed_Z0100.ised',SPS+'_hbBlue_Z0010.ised') # Maraston 2005, kroupa IMF

#print "Using Maraston SPS with kennicutt IMF"
#SPS = "mn05_ssp_ken"
#sps_files = (SPS+'_hbBlue_Z0010.ised', SPS+'_hbRed_Z0100.ised',SPS+'_hbRed_Z0200.ised',SPS+'_hbRed_Z0400.ised') # Maraston 2005, kennicutt IMF

# If you are using Maraston 2005 SPS files. You also need to specify the full path to the stellar.(imf abbreviation) file
stellarmassfile = '/gpfs/data/Galform/Data/stellar_pop_source/Maraston/MN05/standard_2005/other_data/stellarmass.kroupa'
if "mn05" in SPS and "kroupa" in stellarmassfile and not "krp" in SPS:
    print "Warning, using SPS",SPS," with stellarmassfile",stellarmassfile,", this means you should use instantaneous recycling to be consistent"

# The corresponding metallicities for sps_files
#template_Z_grid = np.array([0.05,0.02,0.008,0.004,0.0004]) # Bruzual & Charlot 2003
#template_Z_grid = np.array([0.04,0.02,0.01,0.001]) # Maraston 2005 (kroupa)
template_Z_grid = np.array([0.001, 0.01, 0.02, 0.04]) # Maraston (kennicutt)

# Extra metallicities you would like to include on the template grid that are included using linear interpolation in log(metallicity).
# These values should NOT lie outside the range of metallicities given by the SPS files.
# Note, this line controls whether the extra metallicities are included in the template grid, not whether they are used in the fitting process.

template_Z_grid = np.append(template_Z_grid, np.array([ 0.01, 0.006, 0.003, 0.002, 0.001, 0.0008 ]))





############################ Star formation histories #############################################################


# Assumed SFH type. Default is exp(-t/tau) ==> SFH_Type = ettau . Other options are t exp(-t/tau) where age always = age of universe ==> SFH_Type = tettau
SFH_Type = 'ettau'


#Choose maximum/minimum values for the age grid
age_min = 0.1; age_max_array = age_universe #Gyr

#Grid of e-folding timescales, tau. i.e.  SFR = exp(-t/tau) 
tau_grid = np.array([0.1, 0.3, 0.6, 1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 7.0, 9.0, 13.0, 15.0, 30.0])  #Gyr

#Choose the step size of the extra time steps which improve the SFH integral accuracy. Recommended value is 10^8.
extra_t_stepsize = 10.0**8.0 # yr





############################# Dust Attenuation ###################################################################

# Choose dust attenuation law for use in SED Fitting and for writing the templaes. Options are "Calzetti" and "SlabCalzetti".
# Always use "Calzetti" unless you understand and want to use the other law.
Dust_Law = "Calzetti"


# Choose the template grid of values for the stellar E(B-V) colour excess used in the Calzetti law. 
EBV_grid = np.append(np.array([0.0,0.03,0.06]),np.arange(0.1,1.05,0.05))





############################## Filters and rest/observer frame #####################################################


# Rest-Frame Option. Turning this on will cause the templates to use intrinsic galaxy SEDs that are not shifted into the observer frame before being convolved with filters. This should be turned off by default.
# Note, there is a separate flag that controls whether this is used in the SED fitting.
Rest_Frame = False


# Choose the filters used to perform the fitting
# band          U   B  V  R  I  zA  J  H  K  S3  S4  S5  S8  bJ
#set idband =  (127 51 52 53 54 233 47 48 49 164 165 166 167 6 )

# band    (ACS Bands) BA  Vb  iA  zA
# idband              225 229 232 233

id_band = np.array([ 225,229, 53, 232,  233, 256, 48, 259, 164, 165, 166, 167])
band_list =        ('BA','Vb','R','iA','zA','JI','H','KD','S3','S4','S5','S8')
lambda_eff = np.array([4298,5850,6656,7658,9117,12421,16533,21579,35351,44736,56312,77826]) # rest frame effective wavelengths of each filter

#Choose filter file e.g. Feb08
filter_path = '/cosma/home/d72fqv/Galform-2.5.3/Data/filters/filters_feb08_IRAC_modified.dat'
