#File defining the parameter space that the SED fitting will search over along with various other options
#All of the variables/arrays etc defined in this file are used in Write_Template.py and SED_Fit.py
# This file is not designed to match any particular survey but is just a standard e(-t/tau) grid for comparison with SDSS data

import numpy as np
from scipy import integrate

#Redshift grid
#Note the standard Mill1 output redshifts are:
#z_list = [6.197,4.888,4.179,3.87,3.576,3.06,2.422,2.07,1.504,0.989,0.755,0.509,0.242,0.089,0.0]


z_list = [0.0]

z_array = np.array(z_list)

#Assumed SFH type. Default is exp(-t/tau) - SFH_Type = ettau . Other options are t exp(-t/tau) where age always = age of universe - SFH_Type = tettau
SFH_Type = 'ettau'

#Rest-Frame Option. Turning this on will cause the Write_Template.py script to use intrinsic galaxy SEDs that are not shifted into the observer frame before being convolved with filters. This should be turned off by default.
Rest_Frame = True
print ' '
print "hacked to use rest frame for Cedric's run at z=0"
print ' '

#Choose dust attenuation law for use in SED Fitting. Options are "Calzetti" and "SlabCalzetti".
Dust_Law = "Calzetti"

SPS = 'BC03_pd94_lr_cha'

#It also sets the allowed metallicities that the code searches over.
sps_dir = '/gpfs/data/Galform/Data/stellar_pop/'

sps_files = (SPS+'_Z0500.ised',SPS+'_Z0200.ised',SPS+'_Z0080.ised',SPS+'_Z0040.ised',SPS+'_Z0004.ised')

# If you are using Maraston 2005 SPS files. You also need to specify the full path to the stellar.(imf abbreviation) file
stellarmassfile = '/gpfs/data/Galform/Data/stellar_pop_source/Maraston/MN05/standard_2005/other_data/stellarmass.kroupa'

#The corresponding metallicities for sps_files
template_Z_grid = np.array([0.05,0.02,0.008,0.004,0.0004])

#Extra metallicities you would like to include on the template grid. SEDs calculated via linear interpolation. Comment out the next line if you don't want to do this.
#These values should NOT lie outside the range of metallicities given by the SPS files.

#template_Z_grid = np.append(template_Z_grid, np.array([ 0.01, 0.006, 0.003, 0.002, 0.001, 0.0008 ]))

#Assumed cosmological paramters used to calculate the age of the universe at this redshift. The SED fitting is NOT particularly sensitive to this!
h0 = 0.73 ; omega0 = 0.25 ; lambda0 = 0.75

H = lambda z: 100 * h0 * (omega0*(1.0+z)**3.0 + lambda0)**0.5
age_int = lambda z: 1.0/((1+z)*H(z))

#The age of the universe at this redshift. This is only used to set the
#maximum age that the stellar population of a galaxy is allowed to have.

age_universe = np.zeros(len(z_array))
for n in range(len(z_array)):
    age_universe[n] = integrate.quad(age_int,float(z_array[n]),np.inf)[0] * 979.16


#Choose maximum/minimum values for the age grid
#age_min = 0.001; age_max_array = age_universe #Gyr
age_min = 0.1; age_max_array = age_universe #Gyr

#Grid of e-folding timescales, tau. i.e.  SFR = exp(-t/tau) 
tau_grid = np.array([0.1, 0.3, 0.6, 1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 7.0, 9.0, 13.0, 15.0, 30.0])  #Gyr

#Choose allowed values of the stellar E(B-V) colour excess used in the
#Calzetti law. There is an option to ignore dust effects in the
#Fit_SED python files.

EBV_grid = np.append(np.array([0.0,0.03,0.06]),np.arange(0.1,1.05,0.05))

#Choose the step size of the extra time steps which improve the SFH
#integral accuracy. Recommended value is 10^7. This will be slow though!
extra_t_stepsize = 10.0**8.0 # yr

# u,g,r,i,z photometry
id_band = np.array([ 202, 203, 204, 205, 206])
band_list =        ("uS","gS","rS","iS","zS")

lambda_eff = np.array([3.5513E+03, 4.6862E+03, 6.1658E+03, 7.4812E+03, 8.9317E+03])

#Choose filter file e.g. Feb08
filter_path = '/cosma/home/d72fqv/Galform-2.5.3/Data/filters/filters_feb08_IRAC_modified.dat'
