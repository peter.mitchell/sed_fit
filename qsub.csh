#! /bin/tcsh -f

set logdir = /cosma/home/d72fqv/Python_SAM/Output/LOG
set name = sed_fit
set logfile = $logdir/$name.log

bsub -L /bin/tcsh -P durham  -q cordelia -J $name  -oo $logfile  run_sed_fit.csh
