#! /usr/bin/env python

import sys, math
import numpy as np
import h5py

galaxies_file = None

def Linear_Interpolation_Old(x1_in,y1_in,x2_in):
    '''Perform linear interpolation to get
    y2 values from (x1,y1) for x2 grid of points.
    If y1 has more than one dimension then ensure
    the first y1 axis corresponds to x1.'''

    #Force x1,x2 to be sorted properly 
    x1 = np.sort(x1_in) ; x2 = np.sort(x2_in)

    y1 = y1_in[np.argsort(x1_in)]

    if y1.shape == x1.shape:
        y2 = np.zeros_like(x2)
    else:
        y2_shape = np.append(len(x2),y1.shape[1:])
        y2 = np.zeros(y2_shape)

    for n in range(len(x1)-1):
        #Points that don't need to be interpolated
        y2[ x2 == x1[n] ] = y1[n]

        #Interpolation
        ind = (x2 > x1[n]) & (x2 < x1[n+1])
        if y2[ind].shape[0] == 1:
            y2[ind] = y1[n] + (np.reshape(x2[ind],(len(x2[ind]),1)) - x1[n])*(y1[n+1]-y1[n])/float(x1[n+1]-x1[n])

        #Slightly crazy work-around to counter ANNOYING numpy broadcasting rules
        elif y2[ind].shape[0] > 1:
            x2_shape = (x2[ind].shape[0],)
            for m in range(len(y1.shape)-1):
                x2_shape = x2_shape + (1,)
            y2[ind] = y1[n] + (np.reshape(x2[ind],x2_shape) - x1[n])*(y1[n+1]-y1[n])/float(x1[n+1]-x1[n])

    #Points that don't need to be interpolated
    y2[ x2 == x1[-1] ] = y1[-1]

    #Invert the x2 sort so that x2_in and y2_out correspond to each other.
    y2_out = y2[np.argsort(np.argsort(x2_in))]

    return y2_out

from scipy.interpolate import interp1d

def Linear_Interpolation(x1_in, y1_in, x2_in):
    
    f = interp1d(x1_in, y1_in, kind="linear",axis=0)

    y2_out = f(x2_in)

    return y2_out

def find_nearest(array,value):
    """
    find_nearest(): for a user-specified value, find the
                    nearest corresponding value in an array

    USAGE: nv = find_nearest(array,value)
           array : array to query
           value : value for which we want the nearest value
           nv : nearest value
    
    """
    idx=(np.abs(array-value)).argmin()
    return array[idx]


def find_nearest_arg(array,value):
    """
    find_nearest_arg(): for a user-specified value, find the
                    index of the nearest corresponding value
                    in an array

    USAGE: id = find_nearest(array,value)
           array : array to query
           value : value for which we want the nearest value
           id : index of nearest value
    
    """
    idx = (np.abs(array-value)).argmin()
    return idx



def set_galaxies_file(ifile=None):
    """
    set_galaxies_file(): input name of galaxies.hdf5 file

    USAGE: set_galaxies_file(ifile)

           ifile: name (including directory location) of
                  galaxies.hdf5 file
    """
    if(ifile is None):
        print "*** ERROR: set_galaxies_file():" 
        print "           -- no file specified"
        quit()
    global galaxies_file
    galaxies_file = ifile
    return


def galaxies_file_set(quit_if_None=True):
    """
    galaxies_file_set(): check a galaxies file has been
                           specified

    USAGE: galaxies_file_set([quit_if_None])                      

           quit_if_None: quit if no galaxies file has
                         been specified (default = True)                          
    """
    if(galaxies_file is None):
        if(quit_if_None):
            print "*** ERROR: galaxies_file_set()"
            print "     -- no galaxies.hdf5 file has been specified"
            quit()
        else:
            return False
    else:
        return True
        
            
def nearest_output(z):
    """
    nearest_output(): return the snapshot output number with redshift
                      nearest to user-specified redshift

    USAGE: iout,zout = nearest_output(z)

              z : user-specified redshift
           iout : index of output with redshift closest to z
           zout : redshift of output iout

    Note: outputs are labelled in reverse to Millennium snapshots,
          i.e. iz = 63 --> iout = 1
               iz = 62 --> iout = 2
               etc.

    """
    galaxies_file_set()
    f = h5py.File(galaxies_file,'r')
    zout = np.array(f["/Output_Times/zout"])
    iout = find_nearest_arg(zout,z)
    return iout + 1,zout[iout]

   
def get_parameter(param,quit_if_missing=True):
    """
    get_parameter(): get value of specified parameter in GALFORM run

    USAGE: paramvalue = get_parameter(param,quit_if_missing)

                  param: name of parameter to query
             paramvalue: value of queried parameter
        quit_if_missing: logical variable, quit if parameter
                         cannot be located (default = True)
                         
    """
    galaxies_file_set()
    f = h5py.File(galaxies_file,'r')
    param_names = list(f["/Parameters"])
    if param in param_names:
        paramvalue = f["/Parameters/"+param] 
        if(np.ndim(paramvalue) == 0):
            paramvalue = paramvalue[()]
        else:
            paramvalue = np.array(paramvalue)
        return paramvalue
    else:
        if quit_if_missing:
            print "*** ERROR: get_parameter(): specified parameter cannot"
            print "                        be found in galaxies.hdf5 file"
            print "          specified parameter = ",param
            print "          galaxies file = ",galaxies_file
        else:
            return None

def list_galform_properties(verbose=False):
    """
    list_galform_properties(): return a list of the names of galaxy
                               properties included in the HDF5 file

    USAGE: props = list_galform_properties([verbose])

             props: list of property names
           verbose: print list of property names to screen
                    (default = False)
                               
    """
    galaxies_file_set()
    
    f = h5py.File(galaxies_file,'r')
    
    props = list(f["/Output001"])

    #Remove subdirectories from Output01 list
    if props[0] == "Bands":
        props.remove("Bands")
    if props[0] == "Lines":
        props.remove("Lines")

    if verbose:
        print "*************************"
        print "PROPERTIES IN HDF5 FILE:"
        for p in props:
            print "   ",p
        print "*************************"
    return props
    

def get_galform_property(property,z,repeat=True):
    """
    get_galform_property(): return an array of data for specified
                            property from output with closest
                            redshift value

    USAGE: data = get_galform_property(property,z)

           property : name of property to extract
                  z : redshift at which to extract property
                      (will return property for closest
                      possible redshift to this value)
               data : array of property data (if property does
                      not exist then data = [None])
             repeat : If true, then repeat elements in arrays 
                      such that properties are written out for 
                      each galaxy

    """
    galaxies_file_set()
    props = list_galform_properties()

    iout,zout = nearest_output(z)

    if property not in props:

        if property not in ["itree","jm","jtree","mphalo","ngals","nhalos","weight"]:
            return np.array([None])

    output_name = "Output" + str(iout).rjust(3,"0")
    
    f = h5py.File(galaxies_file,'r')

    index = np.array(f["/"+output_name+"/index"])

    if property not in props:
        prop_data = np.array(f["/"+output_name+"/Trees/"+property])
    else:
        prop_data = np.array(f["/"+output_name+"/"+property])

    if len(prop_data) != len(index) and repeat:

        if "ngals" in props:
            ngals = np.array(f["/"+output_name+"/ngals"])
        else:
            ngals = np.array(f["/"+output_name+"/Trees/ngals"])
        prop_data = np.repeat(prop_data,ngals)
    
    return prop_data

def get_sample_gals_property(property):
     """
     get_sample_gals_property(): return an array of data for specified
                            property from output

    USAGE: data = get_sample_gals_property(property)

           property : name of property to extract

               data : array of property data (if property does
                      not exist then data = [None])

    """
     galaxies_file_set()
     props = list_galform_properties()
     if property not in props:
         return np.array([None])
     output_name = "Output" + str(1).rjust(3,"0")
     f = h5py.File(galaxies_file,'r')  # 'r' specifies that h5py is only to read in hdf5 data, not rewrite it
     prop_data = np.array(f["/"+output_name+"/"+property])

     return prop_data

def snap2z(snap, sim):
    """
    snap2z(): Returns the redshift of a specified snapshot in
              the Millennium Simulation.

    USAGE:  z = snap2z(snap)

            snap : snapshot number (0-63)
            sim  : the simulation type (e.g. 'Mill1','MillGas')
               z :  redshift of specified snapshot              
    """
    snapz = [  1.27000000e+02,   8.00000000e+01,   5.00000000e+01,
               3.00000000e+01,   1.99200000e+01,   1.82400000e+01,
               1.67199990e+01,   1.53400000e+01,   1.40900000e+01,
               1.29400000e+01,   1.19000000e+01,   1.09400000e+01,
               1.00700000e+01,   9.28000000e+00,   8.55000000e+00,
               7.88000000e+00,   7.27000000e+00,   6.71000000e+00,
               6.20000000e+00,   5.72000000e+00,   5.29000000e+00,
               4.89000000e+00,   4.52000000e+00,   4.18000000e+00,
               3.87000000e+00,   3.58000000e+00,   3.31000000e+00,
               3.06000000e+00,   2.83000000e+00,   2.62000000e+00,
               2.42000000e+00,   2.24000000e+00,   2.07000000e+00,
               1.91000000e+00,   1.77000000e+00,   1.63000000e+00,
               1.50000000e+00,   1.39000000e+00,   1.28000000e+00,
               1.17000000e+00,   1.08000000e+00,   9.90000000e-01,
               9.10000000e-01,   8.30000000e-01,   7.60000000e-01,
               6.90000000e-01,   6.20000000e-01,   5.60000000e-01,
               5.10000000e-01,   4.60000000e-01,   4.10000000e-01,
               3.60000000e-01,   3.20000000e-01,   2.80000000e-01,
               2.40000000e-01,   2.10000000e-01,   1.70000000e-01,
               1.40000000e-01,   1.20000000e-01,   9.00000000e-02,
               6.00000000e-02,   4.00000000e-02,   2.00000000e-02,
               0.00000000e+00 ]

    if sim == "Mill1":
        snapz = snapz
    elif sim == "MillGas" or sim == "MillGas_62":
        snapz = snapz[2:]
    elif sim == "EagleDM67":
        if snap == 67:
            return 0.0
        else:
            print "not implemented"
            quit()
    elif sim == "EagleDM":
        snapz = [19.996688, 18.789190, 17.739718, 16.787995, 15.944717, 15.160549, 14.449229,
                 13.804250, 13.203012, 12.658513, 12.150457, 11.676306, 11.233735, 10.827612,
                 10.448525, 10.094723, 9.758225,  9.444298,  9.145538,  8.867005,  8.607494,
                 8.354808,  8.119564,  7.895503,  7.687232, 7.483836, 7.290112, 7.110413,
                 6.934609, 6.767215, 6.607858, 6.456184, 6.311864, 6.174586, 6.039884,
                 5.911805, 5.790076, 5.670491, 5.556895, 5.449051, 5.342981, 5.242353,
                 5.143320, 5.049441, 4.956996, 4.867701, 4.781457, 4.696480, 4.616078,
                 4.536811, 4.458662, 4.383210, 4.310374, 4.240075, 4.170706, 4.102256,
                 4.036203, 3.971005, 3.908105, 3.846000, 3.784681, 3.725538, 3.667125,
                 3.609435, 3.553806, 3.498848, 3.444554, 3.390915, 3.339208, 3.288110,
                 3.237613, 3.187712, 3.138398, 3.090876, 3.042702, 2.996279, 2.950389,
                 2.903869, 2.859041, 2.814726, 2.770921, 2.728723, 2.685905, 2.643579,
                 2.601740, 2.560380, 2.520538, 2.480111, 2.440148, 2.401652, 2.362590,
                 2.323976, 2.285807, 2.249037, 2.211728, 2.174847, 2.138854, 2.102810,
                 2.067180, 2.031959, 1.997142, 1.962287, 1.928270, 1.894645, 1.860981,
                 1.827709, 1.795238, 1.762731, 1.730601, 1.698446, 1.667064, 1.635657,
                 1.605006, 1.574330, 1.544015, 1.514057, 1.484451, 1.455195, 1.425923,
                 1.397356, 1.368774, 1.340533, 1.312629, 1.285057, 1.257814, 1.230566,
                 1.203972, 1.177374, 1.151415, 1.125450, 1.099799, 1.074458, 1.049119,
                 1.024389, 0.999958, 0.975529, 0.951687, 0.927848, 0.904299, 0.881317,
                 0.858337, 0.835910, 0.813753, 0.791864, 0.770239, 0.748875, 0.727768,
                 0.707296, 0.686941, 0.666953, 0.647201, 0.627804, 0.608754, 0.590045,
                 0.571553, 0.553392, 0.535554, 0.518033, 0.500712, 0.483809, 0.467205,
                 0.450786, 0.434657, 0.418918, 0.403351, 0.388161, 0.373135, 0.358473,
                 0.343968, 0.329814, 0.315907, 0.302241, 0.288813, 0.275618, 0.262651,
                 0.249909, 0.237479, 0.225262, 0.213257, 0.201369, 0.189773, 0.178464,
                 0.167263, 0.156253, 0.145432, 0.134881, 0.124427, 0.114234, 0.104133,
                 0.094286, 0.084527, 0.075014, 0.065584, 0.056394, 0.047282, 0.038327,
                 0.029600, 0.020946, 0.012441, 0.004080, 0.000000]
    else:
        from snapz_nonstandard import *
        if sim == "MillGas_123":
            snapz = MillGas_123()
        elif sim == "MillGas_245":
            snapz = MillGas_245()
        elif sim == "MillGas_489":
            snapz = MillGas_489()
        elif sim == "MillGas_977":
            snapz = MillGas_977()
        else:
            print "Error: sim = ", sim, " is not a valid imput for snap2z in utilities_galform.py"
            exit()

    return snapz[snap]


def z2snap(z,sim):
    """
    z2snap(): Returns the number of the snapshot whose redshift
              is closest to the user-specified value.

    USAGE:  snap = z2snap(z)

             z  : user-specified redshift
            sim : user-specified simulation type e.g. (Mill1, MillGas)
           snap : snapshot number (0-63) closest to redshift, z 
              
    """
    snapz = [  1.27000000e+02,   8.00000000e+01,   5.00000000e+01,
               3.00000000e+01,   1.99200000e+01,   1.82400000e+01,
               1.67199990e+01,   1.53400000e+01,   1.40900000e+01,
               1.29400000e+01,   1.19000000e+01,   1.09400000e+01,
               1.00700000e+01,   9.28000000e+00,   8.55000000e+00,
               7.88000000e+00,   7.27000000e+00,   6.71000000e+00,
               6.20000000e+00,   5.72000000e+00,   5.29000000e+00,
               4.89000000e+00,   4.52000000e+00,   4.18000000e+00,
               3.87000000e+00,   3.58000000e+00,   3.31000000e+00,
               3.06000000e+00,   2.83000000e+00,   2.62000000e+00,
               2.42000000e+00,   2.24000000e+00,   2.07000000e+00,
               1.91000000e+00,   1.77000000e+00,   1.63000000e+00,
               1.50000000e+00,   1.39000000e+00,   1.28000000e+00,
               1.17000000e+00,   1.08000000e+00,   9.90000000e-01,
               9.10000000e-01,   8.30000000e-01,   7.60000000e-01,
               6.90000000e-01,   6.20000000e-01,   5.60000000e-01,
               5.10000000e-01,   4.60000000e-01,   4.10000000e-01,
               3.60000000e-01,   3.20000000e-01,   2.80000000e-01,
               2.40000000e-01,   2.10000000e-01,   1.70000000e-01,
               1.40000000e-01,   1.20000000e-01,   9.00000000e-02,
               6.00000000e-02,   4.00000000e-02,   2.00000000e-02,
               0.00000000e+00 ]
    if sim == "Mill1":
        snapz = snapz
    elif sim == "MillGas" or sim == "MillGas_62":
        snapz = snapz[2:]
    elif sim == "EagleDM67":
        if z == 0.0:
            return 67
        else:
            print "not implemented"
            quit()
    elif sim == "EagleDM":
        snapz = [19.996688, 18.789190, 17.739718, 16.787995, 15.944717, 15.160549, 14.449229,
                 13.804250, 13.203012, 12.658513, 12.150457, 11.676306, 11.233735, 10.827612,
                 10.448525, 10.094723, 9.758225,  9.444298,  9.145538,  8.867005,  8.607494,
                 8.354808,  8.119564,  7.895503,  7.687232, 7.483836, 7.290112, 7.110413,
                 6.934609, 6.767215, 6.607858, 6.456184, 6.311864, 6.174586, 6.039884,
                 5.911805, 5.790076, 5.670491, 5.556895, 5.449051, 5.342981, 5.242353,
                 5.143320, 5.049441, 4.956996, 4.867701, 4.781457, 4.696480, 4.616078,
                 4.536811, 4.458662, 4.383210, 4.310374, 4.240075, 4.170706, 4.102256,
                 4.036203, 3.971005, 3.908105, 3.846000, 3.784681, 3.725538, 3.667125,
                 3.609435, 3.553806, 3.498848, 3.444554, 3.390915, 3.339208, 3.288110,
                 3.237613, 3.187712, 3.138398, 3.090876, 3.042702, 2.996279, 2.950389,
                 2.903869, 2.859041, 2.814726, 2.770921, 2.728723, 2.685905, 2.643579,
                 2.601740, 2.560380, 2.520538, 2.480111, 2.440148, 2.401652, 2.362590,
                 2.323976, 2.285807, 2.249037, 2.211728, 2.174847, 2.138854, 2.102810,
                 2.067180, 2.031959, 1.997142, 1.962287, 1.928270, 1.894645, 1.860981,
                 1.827709, 1.795238, 1.762731, 1.730601, 1.698446, 1.667064, 1.635657,
                 1.605006, 1.574330, 1.544015, 1.514057, 1.484451, 1.455195, 1.425923,
                 1.397356, 1.368774, 1.340533, 1.312629, 1.285057, 1.257814, 1.230566,
                 1.203972, 1.177374, 1.151415, 1.125450, 1.099799, 1.074458, 1.049119,
                 1.024389, 0.999958, 0.975529, 0.951687, 0.927848, 0.904299, 0.881317,
                 0.858337, 0.835910, 0.813753, 0.791864, 0.770239, 0.748875, 0.727768,
                 0.707296, 0.686941, 0.666953, 0.647201, 0.627804, 0.608754, 0.590045,
                 0.571553, 0.553392, 0.535554, 0.518033, 0.500712, 0.483809, 0.467205,
                 0.450786, 0.434657, 0.418918, 0.403351, 0.388161, 0.373135, 0.358473,
                 0.343968, 0.329814, 0.315907, 0.302241, 0.288813, 0.275618, 0.262651,
                 0.249909, 0.237479, 0.225262, 0.213257, 0.201369, 0.189773, 0.178464,
                 0.167263, 0.156253, 0.145432, 0.134881, 0.124427, 0.114234, 0.104133,
                 0.094286, 0.084527, 0.075014, 0.065584, 0.056394, 0.047282, 0.038327,
                 0.029600, 0.020946, 0.012441, 0.004080, 0.000000]
    else:
        from snapz_nonstandard import *
        if sim == "MillGas_123":
            snapz = MillGas_123()
        elif sim == "MillGas_245":
            snapz = MillGas_245()
        elif sim == "MillGas_489":
            snapz = MillGas_489()
        elif sim == "MillGas_977":
            snapz = MillGas_977()
        else:
            print "Error: sim = ", sim, " is not a valid imput for z2snap in utilities_galform.py"
            exit()

    snap = find_nearest_arg(np.array(snapz),z)
    
    return snap


def subvol_volume(ivol):
    """
    subvol_volume(): Returns the volume, in (Mpc/h)^3, of the
                     specified subvolume.

    USAGE:  V = subvol_volume(ivol)

            ivol : ID of subvolume (0-511)
            V    : volume of subvolume 'ivol' in (Mpc/h)^3
    
    """
    volumes = [ 213599. ,  305145. ,  335660. ,  308959. ,  251743. ,  217413. ,
                198341. ,  267001. ,  286073. ,  198341. ,  289887. ,  148754. ,
                251743. ,  240300. ,  213599. ,  305145. ,  213599. ,  160197. ,
                232671. ,  278444. ,  209785. ,  270815. ,  244114. ,  217413. ,
                289887. ,  286073. ,  301330. ,  297516. ,  331845. ,  286073. ,
                297516. ,  198341. ,  240300. ,  202156. ,  278444. ,  305145. ,
                198341. ,  232671. ,  240300. ,  240300. ,  213599. ,  205970. ,
                221228. ,  259372. ,  263186. ,  244114. ,  156383. ,  175455. ,
                232671. ,  312773. ,  221228. ,  247929. ,  282258. ,  217413. ,
                217413. ,  225042. ,  259372. ,  362361. ,  221228. ,  316588. ,
                289887. ,  251743. ,  312773. ,  347103. ,  354732. ,  175455. ,
                251743. ,  320402. ,  236485. ,  278444. ,  156383. ,  209785. ,
                320402. ,  259372. ,  289887. ,  194527. ,  217413. ,  217413. ,
                282258. ,  305145. ,  213599. ,  225042. ,  289887. ,  263186. ,
                308959. ,  240300. ,  221228. ,  183084. ,  278444. ,  247929. ,
                209785. ,  228857. ,  194527. ,  247929. ,  263186. ,  225042. ,
                217413. ,  301330. ,  251743. ,  221228. ,  293701. ,  209785. ,
                133497. ,  167826. ,  213599. ,  331845. ,  194527. ,  144940. ,
                213599. ,  217413. ,  274629. ,  240300. ,  286073. ,  209785. ,
                213599. ,  301330. ,  328031. ,  339474. ,  289887. ,  335660. ,
                362361. ,  263186. ,  171641. ,  194527. ,  141125. ,  186898. ,
                186898. ,  274629. ,  244114. ,  152569. ,  270815. ,  350917. ,
                217413. ,  240300. ,  225042. ,  225042. ,  328031. ,  213599. ,
                232671. ,  278444. ,  202156. ,  305145. ,  183084. ,  278444. ,
                274629. ,  141125. ,  213599. ,  259372. ,  240300. ,  213599. ,
                205970. ,  225042. ,  209785. ,  251743. ,  217413. ,  228857. ,
                225042. ,  377618. ,  236485. ,  251743. ,  236485. ,  190713. ,
                221228. ,  236485. ,  221228. ,  267001. ,  217413. ,  164012. ,
                255557. ,  278444. ,  217413. ,  217413. ,  247929. ,  213599. ,
                316588. ,  297516. ,  324217. ,  282258. ,  247929. ,  240300. ,
                179269. ,  213599. ,  244114. ,  297516. ,  213599. ,  221228. ,
                167826. ,  236485. ,  263186. ,  175455. ,  213599. ,  202156. ,
                259372. ,  240300. ,  183084. ,  335660. ,  236485. ,  228857. ,
                259372. ,  202156. ,  205970. ,  289887. ,  213599. ,  221228. ,
                125868. ,  278444. ,  217413. ,  305145. ,  225042. ,  278444. ,
                274629. ,  91538.2 ,  225042. ,  190713. ,  240300. ,  198341. ,
                236485. ,  232671. ,  267001. ,  175455. ,  164012. ,  183084. ,
                198341. ,  236485. ,  205970. ,  205970. ,  190713. ,  240300. ,
                270815. ,  148754. ,  217413. ,  228857. ,  236485. ,  221228. ,
                194527. ,  278444. ,  186898. ,  205970. ,  213599. ,  209785. ,
                324217. ,  278444. ,  247929. ,  141125. ,  232671. ,  282258. ,
                213599. ,  198341. ,  202156. ,  259372. ,  217413. ,  194527. ,
                186898. ,  217413. ,  213599. ,  251743. ,  293701. ,  228857. ,
                301330. ,  263186. ,  289887. ,  259372. ,  251743. ,  251743. ,
                297516. ,  247929. ,  175455. ,  137311. ,  202156. ,  221228. ,
                179269. ,  244114. ,  244114. ,  270815. ,  274629. ,  259372. ,
                259372. ,  259372. ,  175455. ,  270815. ,  312773. ,  263186. ,
                221228. ,  232671. ,  335660. ,  320402. ,  209785. ,  228857. ,
                301330. ,  236485. ,  205970. ,  373804. ,  331845. ,  278444. ,
                244114. ,  217413. ,  259372. ,  255557. ,  247929. ,  186898. ,
                194527. ,  305145. ,  301330. ,  202156. ,  305145. ,  255557. ,
                369989. ,  267001. ,  259372. ,  274629. ,  240300. ,  152569. ,
                343289. ,  194527. ,  190713. ,  152569. ,  312773. ,  205970. ,
                282258. ,  282258. ,  301330. ,  225042. ,  301330. ,  267001. ,
                274629. ,  278444. ,  236485. ,  289887. ,  251743. ,  164012. ,
                202156. ,  194527. ,  267001. ,  316588. ,  228857. ,  236485. ,
                202156. ,  194527. ,  221228. ,  202156. ,  263186. ,  240300. ,
                247929. ,  179269. ,  255557. ,  274629. ,  301330. ,  240300. ,
                320402. ,  293701. ,  259372. ,  232671. ,  289887. ,  213599. ,
                259372. ,  213599. ,  282258. ,  259372. ,  297516. ,  186898. ,
                205970. ,  228857. ,  225042. ,  167826. ,  213599. ,  255557. ,
                270815. ,  194527. ,  202156. ,  217413. ,  255557. ,  156383. ,
                247929. ,  312773. ,  286073. ,  267001. ,  225042. ,  228857. ,
                240300. ,  270815. ,  267001. ,  263186. ,  244114. ,  270815. ,
                286073. ,  305145. ,  221228. ,  396690. ,  125868. ,  205970. ,
                179269. ,  373804. ,  289887. ,  274629. ,  244114. ,  263186. ,
                301330. ,  308959. ,  247929. ,  213599. ,  213599. ,  308959. ,
                308959. ,  259372. ,  263186. ,  289887. ,  232671. ,  164012. ,
                225042. ,  186898. ,  282258. ,  263186. ,  186898. ,  263186. ,
                240300. ,  301330. ,  270815. ,  267001. ,  205970. ,  339474. ,
                286073. ,  228857. ,  274629. ,  278444. ,  289887. ,  228857. ,
                232671. ,  251743. ,  240300. ,  164012. ,  179269. ,  160197. ,
                217413. ,  244114. ,  328031. ,  213599. ,  183084. ,  167826. ,
                186898. ,  228857. ,  209785. ,  270815. ,  297516. ,  194527. ,
                251743. ,  328031. ,  335660. ,  305145. ,  141125. ,  144940. ,
                244114. ,  335660. ,  328031. ,  232671. ,  255557. ,  198341. ,
                301330. ,  263186. ,  278444. ,  194527. ,  267001. ,  213599. ,
                274629. ,  270815. ,  251743. ,  205970. ,  267001. ,  255557. ,
                267001. ,  198341. ,  244114. ,  183084. ,  221228. ,  202156. ,
                267001. ,  312773. ,  335660. ,  236485. ,  225042. ,  205970. ,
                225042. ,  209785. ,  259372. ,  186898. ,  236485. ,  205970. ,
                301330. ,  228857. ,  263186. ,  244114. ,  236485. ,  339474. ,
                339474. ,  301330. ,  358546. ,  259372. ,  209785. ,  137311. ,
                194527. ,  286073. ,  194527. ,  186898. ,  205970. ,  183084. ,
                167826. ,  312773. ,  289887. ,  255557. ,  228857. ,  259372. ,
                301330. ,  221228. ]

    return volumes[ivol]

def read_galform_subvolumes(galformdir=None,model=None,zout=None,sample_file=None,\
                            subvolumes=None,sim=None,props=None,\
                            galform_type=None,zfinal=0,repeat=True,verbose=False,violeta=False,tree_version="old"):
    """
    read_galform_subvolumes(): Read data from GALFORM subvolume directories -- either
                               read directly from galaxies.hdf5 file or read a
                               SAMPLE_GALS ascii file.

    USAGE: data = read_galform_subvolumes(model,[zout],[subvolumes],[sample_file],
                                            [props],[galform_type])

           galform dir = Path of the galform directory
                 model = Name of GALFORM model
                 zout  = Redshift of GALFORM output (used either to select snapshot
                         directory or select output directory inside hdf5 file).
                         If zout=None (default), will assume zout = 0.0.
            subvolumes = List of subvolumes to read. If subvolumes=None (default)
                         then will read the default 10 Millennium subvolumes. (Subvolumes
                         ignored if reading Monte Carlo output).
           sample_file = Name of SAMPLE_GALS ascii/hdf5 file to read. If sample_file=None
                         (default), will read data directly from galaxies.hdf5 file.
                 props = List of properties to extract from GALFORM output. If
                         props=None (default), will extract all properties.
          galform_type = The type of Galform output -- 'nbody','mctree','lightcone',
                         'fof'.
                   sim = The desired simulation. e.g. "Mill1", "MillGas", "Mill2"
                  data = Dictionary containing galaxies data. Dictionary keys are
                         list of properties.
 
    """

    # Check necessary inputs have been specified
    if model is None:
        print "*** ERROR read_galform_subvolumes(): no model specified!"
        quit()
    # Check optional inputs
    if zout is None:
        print "WARNING read_galform_subvolumes(): zout = None -- assuming zout = 0.0"
        zout = 0.0
    if subvolumes is None and galform_type != 'mctree':
        print "WARNING read_galform_subvolumes(): subvolumes = None "
        print "             -- assuming all default subvolumes are to be used"
        if sim == "Mill1":
            if tree_version=="old":
                subvolumes = [3,31,47,229,231,266,334,381,424]
            else:
                subvolumes = [0]
        elif "MillGas" in sim:
            subvolumes = [0]
        elif sim is not None:
            print "Error in read_galform_subvolumes(): sim = "+ sim +" either does not exist or has not yet been implemented" 
            quit()
    if subvolumes == "Large" and galform_type != 'mctree':
        if sim == "Mill1":
            subvolumes = Large_Subvolume_List()
        else:
            print "Error: Subvolumes = Large is only implemented for Mill1 simulation"
            quit()
    if galformdir is None:
        print "Error: read_galform_subvolumes(): galformdir=None - No directory path specified!"
        quit()
    if galform_type is None:
        print "Warning: read_galform_subvolumes(): galform_type = None"
        print "Valid options are 'nbody','mctree'"
        print "Assuming galform_type = 'nbody'"
        galform_type = 'nbody'
    if sim is None and galform_type is "nbody":
        print "Error: read_galform_subvolumes(): sim=None but galform_type='nbody' - no simulation specified!"
        quit()
    if props is None:
        print "Warning: read_galform_subvolumes(): props = None - assuming you want everything from the file"
    if verbose:
        print "read_galform_subvolumes:"
        print "MODEL = ",model
        print "REDSHIFT = ",zout
    
    # Set up name/location of GALFORM data depending on GALFORM type
    if galform_type.lower() == "lightcone":
        outdir = lcone_dir + model + "/ivol_"
        use_hdf5 = True
        fname = "galaxies.hdf5"
    elif galform_type.lower() == "nbody" or galform_type.lower()=="aquarius_trees":
        iz = z2snap(zout,sim)
        if snap2z(iz,sim) != zout:
            print "Warning: The desired redshift z="+str(zout)+" is not possible for sim="+sim+" Using "+str(snap2z(iz,sim))+" instead"
        output_dir = galformdir + "Nbody/" + sim + "/" + model + "/iz" + str(iz) + "/ivol"
        if galform_type.lower()=="aquarius_trees":
            output_dir = galformdir + "aquarius_trees/" + sim + "/" + model + "/iz" + str(iz) + "/ivol"
        if violeta == True:
            output_dir = galformdir + sim + "/" + model + "/iz" + str(iz) + "/ivol"
        if violeta == "will":
            output_dir = galformdir + sim + "/" + model + "/iz" + str(iz) + "/ivol"
        if sample_file is None:
            use_hdf5 = True
            fname = "galaxies.hdf5"
        else:
            use_hdf5 = False
            fname = sample_file
            if ".hdf5" in fname:
                use_hdf5 = True
        if "Track_Halos" in output_dir:
            galformdir_edit = galformdir[0:-11]
            output_dir = galformdir_edit + "Nbody/" + sim + "/" + model + "/Track_Halos/izfinal"+str(iz)+"/ivol"
            if zfinal != zout:
                izfinal = z2snap(zfinal,sim)
                print "Outputting for z = ",zout, " but for iz_final at z = ",zfinal
                output_dir = galformdir_edit + "Nbody/" + sim + "/" + model + "/Track_Halos/izfinal"+str(izfinal)+"/ivol"

    elif galform_type.lower() == "mctree":
        output_dir = galformdir + "MCtree/" + model + "/z" + str(zout)
        if sample_file is None:
            use_hdf5 = True
            fname = "galaxies.hdf5"
        else:
            use_hdf5 = False
            fname = sample_file
            if ".hdf5" in fname:
                use_hdf5 = True
        subvolumes = [""]
        if "Track_Halos" in output_dir:
            galformdir_edit = galformdir[0:-11]
            if int(zfinal) == zfinal:
                output_dir = galformdir_edit + "MCtree/" + model + "/Track_Halos/zfinal"+str(int(zfinal))
            else:
                output_dir = galformdir_edit + "MCtree/" + model + "/Track_Halos/zfinal"+str(zfinal)
            if zfinal != zout:
                print "Outputting for z = ",zout, " but for iz_final at z = ",zfinal
    elif galform_type.lower() == "fof":
        output_dir = mctree_dir + model + "/z" + str(zout) + "/ivol"
        if sample_file is None:
            use_hdf5 = True
            fname = "galaxies.hdf5"
        else:
            use_hdf5 = False
            fname = sample_file   

    data = None    # Intialise data dictionary
    # Loop over list of Millennium subvolumes
    first_read = True
    checked_properties = False

    for ivol in subvolumes:

        ifile = output_dir + str(ivol) + "/" + fname

        if verbose:
            print "reading file: ",ifile
        if use_hdf5:
            set_galaxies_file(ifile)
            if props is None:
                props = list_galform_properties()
                if "redshift" in props:
                    props.remove("redshift")
                #Remove any non-array objects from props
            # Check file is not empty -- skip empty files
            if sample_file is None or sample_file == "galaxies.hdf5":
                test = get_galform_property(props[0],zout)
            else:
                test = get_sample_gals_property(props[0])
            
            if test[0] is None:
                #ngalaxies.append(0)
                continue

            else:

                # Number of galaxies
                ngals = len(test)
                # Check properties are in GALFORM output --
                # skip those properties that are missing
                if not checked_properties:
                    galform_props = list_galform_properties()

                    if "ivol" in props:
                        galform_props.append("ivol")
                    if "ivolID" in props:
                        galform_props.append("ivolID")
                    for iprop in props:
                        if iprop not in galform_props:
                            if iprop not in ["itree","jm","jtree","mphalo","ngals","nhalos","weight"]:
                                print "*** WARNING: read_galform_subvolumes():"
                                print "            ",iprop," not found in GALFORM output!"
                                print "       --> this property will be skipped"
                                props.remove(iprop)
                    checked_properties = True
                # Loop over properties -- store in data dictionary

                for iprop in props:
                    if iprop == "ivol":
                        propdata = np.ones(ngals,int)*ivol
                    elif iprop == "ivolID":
                        propdata = np.arange(ngals) + 1
                    elif sample_file is None or sample_file == "galaxies.hdf5":
                        propdata = np.copy(get_galform_property(iprop,zout,repeat=repeat))
                    else:
                        propdata = np.copy(get_sample_gals_property(iprop))
                    if iprop == "weight" and galform_type.lower() == "nbody":
                        propdata *= 1.0/float(len(subvolumes))
                    if isinstance(propdata[0],float):
                        propdata = propdata.astype('float64')
                    if first_read:
                        if data is None: # Intialise dictionary
                            data = dict([(iprop,np.copy(propdata))])
                        else:
                            data[iprop] = np.copy(propdata)
                    else:
                        if data is None: # Intialise dictionary
                            data = dict([(iprop,np.copy(propdata))])
                        else:
                            data[iprop] = np.append(data[iprop],propdata)
                first_read = False
                    
        else:
            # Check ascii file exists
            if file_exists(ifile):
                # Check ascii file contains data
                test = get_property(ifile,props[0])
                if not found_data():
                    #ngalaxies.append(0)
                    continue
                else:
                    # Number of galaxies
                    ngals = len(test)
                    # Check properties are in GALFORM output --
                    # skip those properties that are missing
                    if not checked_properties:
                        galform_props = extract_property_list(ifile)
                        if "ivol" in props:
                            galform_props.append("ivol")
                        if "ivolID" in props:
                            galform_props.append("ivolID")
                        for iprop in props:
                            if iprop not in galform_props:
                                print "*** WARNING: read_galform_subvolumes():"
                                print "            ",iprop," not found in GALFORM output!"
                                print "       --> this property will be skipped"
                                props.remove(iprop)
                        checked_properties = True
                    # Loop over properties -- store in data dictionary
                    for iprop in props:
                        dtype = float
                        if iprop in integer_props:
                            dtype = int
                        if iprop in long_props:
                            dtype = long
                        if iprop == "ivol":
                            propdata = np.ones(ngals,int)*ivol
                        elif iprop == "ivolID":
                            propdata = np.arange(ngals) + 1
                        else:
                            propdata = np.copy(get_property(ifile,iprop,dtype))
                        if isinstance(propdata,float):
                            propdata = propdata.astype('float64')
                        if first_read:
                            if data is None: # Intialise dictionary
                                data = dict([(iprop,np.copy(propdata))])
                            else:
                                data[iprop] = np.copy(propdata)
                        else:
                            if data is None: # Intialise dictionary
                                data = dict([(iprop,np.copy(propdata))])
                            else:
                                data[iprop] = np.append(data[iprop],propdata)
                    first_read = False
    # Return data dictionary
    return data

def get_cosmological_parameters(model,tree,sim,galformdir,zout,violeta=False,subvolume=None,tree_version="old"):
    '''get_cosmological_parameters(): Returns h, omega_matter and omega_lambda for a given model
    Usage: model = model name
           tree  = tree type (e.g. 'mctree')
           sim   = simulation type if nbody (e.g. 'Mill1')
     galformdir  = galform output path
           zout  = output redshift
       subvolume = subvolume number (integer) if using nbody trees
              h  = hubbles constant in 100kms-1Mpc-1
             omm = omega matter
             oml = omega lambda
    tree version = merger tree version, needed to know if we are using the ivol 1-512 or 0-63 convention'''

    if tree.lower() == "nbody" or tree.lower() == "aquarius_trees":
        iz = z2snap(zout,sim)
        output_dir = galformdir + "Nbody/" + sim + "/" + model + "/iz" + str(iz) + "/ivol"
        if violeta == True:
            output_dir = galformdir + sim + "/" + model + "/iz" + str(iz) + "/ivol"
        elif violeta == "will":
            output_dir = galformdir + sim + "/" + model + "/iz" + str(iz) + "/ivol"
        if "Track_Halos" in output_dir:
            galformdir_edit = galformdir[0:-11]
            output_dir = galformdir_edit + "Nbody/" + sim + "/" + model + "/Track_Halos/izfinal"+str(iz)+"/ivol"
        if subvolume is None:
            if sim == "Mill1" and tree_version == "old":
                output_dir += "3"
            elif sim == "MillGas" or tree_version == "new" or sim == "EagleDM":
                output_dir += "0"
            else:
                print "Error in get_cosmological_parameters(): sim = "+ sim +" either does not exist or has not yet been implemented"
                quit()
        else:
            output_dir += str(subvolume)

    elif tree.lower() == "mctree":
        output_dir = galformdir + "MCtree/" + model + "/z" + str(zout)
        if "Track_Halos" in output_dir:
            galformdir_edit = galformdir[0:-11]
            if int(zout) == zout:
                output_dir = galformdir_edit + "MCtree/" + model + "/Track_Halos/zfinal"+str(int(zout))
            else:
                output_dir = galformdir_edit + "MCtree/" + model + "/Track_Halos/zfinal"+str(zout)
    else:
        print "Error: tree = "+tree+" type not recognised! Valid options are 'mctree' or 'nbody'"
        quit()

    ifile = output_dir + "/used-parameters"
    parameter_file = open(ifile,'r')
    for line in parameter_file:
        if line.find("h0") != -1:
            h = float(line.split()[2])
        if line.find("omega0") != -1:
            omm = float(line.split()[2])
        if line.find("lambda0") != -1:
            oml = float(line.split()[2])
        if line.find("omegab") != -1:
            omb = float(line.split()[2])
    parameter_file.close()
    return h, omm, oml, omb

def get_recycled_fraction_yield(model,tree,sim,galformdir,zout,violeta=False,subvolume=None,tree_version="old"):
    '''get_recycled_fraction_yield(): Returns recycled fraction and yield for a given model
    Note for multiple IMF models, only the disk recycled fraction/yield is returned
    Usage: model = model name
           tree  = tree type (e.g. 'mctree')
           sim   = simulation type if nbody (e.g. 'Mill1')
     galformdir  = galform output path
           zout  = output redshift'''

    if tree.lower() == "nbody":
        iz = z2snap(zout,sim)
        output_dir = galformdir + "Nbody/" + sim + "/" + model + "/iz" + str(iz) + "/ivol"
        if violeta == True:
            output_dir = galformdir + sim + "/" + model + "/iz" + str(iz) + "/ivol"
        elif violeta == "will":
            output_dir = galformdir + sim + "/" + model + "/iz" + str(iz) + "/ivol"

        if "Track_Halos" in output_dir:
            galformdir_edit = galformdir[0:-11]
            output_dir = galformdir_edit + "Nbody/" + sim + "/" + model + "/Track_Halos/izfinal"+str(iz)+"/ivol"

        if subvolume is None:
            if sim == "Mill1" and tree_version == "old":
                output_dir += "3"
            elif sim == "MillGas" or tree_version == "new":
                output_dir += "0"
            else:
                print "Error: sim = "+ sim +" either does not exist or has not yet been implemented"
                quit()
        else:
            output_dir += str(subvolume)

    elif tree.lower() == "mctree":
        output_dir = galformdir + "MCtree/" + model + "/z" + str(zout)
        if "Track_Halos" in output_dir:
            galformdir_edit = galformdir[0:-11]
            if int(zout) == zout:
                output_dir = galformdir_edit + "MCtree/" + model + "/Track_Halos/zfinal"+str(int(zout))
            else:
                output_dir = galformdir_edit + "MCtree/" + model + "/Track_Halos/zfinal"+str(zout)
    else:
        print "Error: tree = "+tree+" type not recognised! Valid options are 'mctree' or 'nbody'"
        quit()

    ifile = output_dir + "/used-parameters"
    parameter_file = open(ifile,'r')
    for line in parameter_file:
        if line.find("recycle") != -1 and line.find("burst_recycle") == -1:
            R = float(line.split()[2])
        if line.find("yield") != -1 and line.find("burst_yield") == -1:
            y = float(line.split()[2])
    parameter_file.close()
    return R, y

def get_output_times(model,tree,sim,galformdir,zout,subvolume=None,tree_version="old"):
    '''get_output_times(): Returns output times (both age and lookback times) in Gyr
    Usage: model = model name
           tree  = tree type (e.g. 'mctree')
           sim   = simulation type if nbody (e.g. 'Mill1')
     galformdir  = galform output path
           zout  = output redshift'''

    if tree.lower() == "nbody":
        iz = z2snap(zout,sim)
        output_dir = galformdir + "Nbody/" + sim + "/" + model + "/iz" + str(iz) + "/ivol"
        if "Track_Halos" in output_dir:
            galformdir_edit = galformdir[0:-11]
            output_dir = galformdir_edit + "Nbody/" + sim + "/" + model + "/Track_Halos/izfinal"+str(iz)+"/ivol"
        
        if subvolume is None:
            if sim == "Mill1" and tree_version == "old":
                output_dir += "3"
            elif sim == "MillGas" or tree_version == "new":
                output_dir += "0"
            else:
                print "Error: sim = "+ sim +" either does not exist or has not yet been implemented"
                quit()
        else:
            output_dir += str(subvolume)

    elif tree.lower() == "mctree":
        output_dir = galformdir + "MCtree/" + model + "/z" + str(zout)
        if "Track_Halos" in output_dir:
            galformdir_edit = galformdir[0:-11]
            if int(zout) == zout:
                output_dir = galformdir_edit + "MCtree/" + model + "/Track_Halos/zfinal"+str(int(zout))
            else:
                output_dir = galformdir_edit + "MCtree/" + model + "/Track_Halos/zfinal"+str(zout)
    else:
        print "Error: tree = "+tree+" type not recognised! Valid options are 'mctree' or 'nbody'"
        quit()

    ifile = output_dir + "/output.times"
    tage,tlb = np.loadtxt(ifile,usecols=(3,4),unpack=True)

    return tage, tlb

def get_dustpars(model,tree,sim,galformdir,zout,tree_version="old"):
    '''get_dustpars(): Returns dust modelling parameters
    Usage: model = model name
           tree  = tree type (e.g. 'mctree')
           sim   = simulation type if nbody (e.g. 'Mill1')
     galformdir  = galform output path
           zout  = output redshift
      beta_disk  = dust emission spectral index for quiescently star-forming disks
      beta_burst = dust emission spectral index for starbursts
   lambda_b_disk = break wavelength in FIR SED for quiescently star-forming disks
 lambda_b_burst  = break wavelength in FIR SED for starbursts
          fcloud = fraction of dust mass in compact dust clouds'''

    if tree.lower() == "nbody":
        iz = z2snap(zout,sim)
        if sim == "Mill1" and tree_version=="old":
            output_dir = galformdir + "Nbody/" + sim + "/" + model + "/iz" + str(iz) + "/ivol3"
        elif sim == "MillGas" or tree_version =="new":
            output_dir = galformdir + "Nbody/" + sim + "/" + model + "/iz" + str(iz) + "/ivol0"
        else:
            print "Error: sim = "+ sim +" either does not exist or has not yet been implemented"
            quit()
    elif tree.lower() == "mctree":
        output_dir = galformdir + "MCtree/" + model + "/z" + str(zout)
        if "Track_Halos" in output_dir:
            galformdir_edit = galformdir[0:-11]
            if int(zout) == zout:
                output_dir = galformdir_edit + "MCtree/" + model + "/Track_Halos/zfinal"+str(int(zout))
            else:
                output_dir = galformdir_edit + "MCtree/" + model + "/Track_Halos/zfinal"+str(zout)
    else:
        print "Error: tree = "+tree+" type not recognised! Valid options are 'mctree' or 'nbody'"
        quit()

    ifile = output_dir + "/dustpars"
    parameter_file = open(ifile,'r')
    for line in parameter_file:
        if line.find("beta2_disk") != -1:
            beta_disk = float(line.split()[2])
        if line.find("beta2_burst") != -1:
            beta_burst = float(line.split()[2])
        if line.find("lambda_break_burst") != -1:
            lambda_b_burst = float(line.split()[2])
        if line.find("lambda_break_disk") != -1:
            lambda_b_disk = float(line.split()[2])
        if line.find("fcloud") != -1:
            fcloud = float(line.split()[2])
    parameter_file.close()
    return beta_disk, lambda_b_disk, beta_burst, lambda_b_burst, fcloud

def get_galform_parameters(parameter_list,model,tree,sim,galformdir,zout,violeta=False,tree_version="old"):
    '''get_galform_parameters(): Returns a list of arbitrary galform input parameters from the used-parameters file
    Usage: 
  parameter_list = list of the names of parameters to be returned
           model = model name
           tree  = tree type (e.g. 'mctree')
           sim   = simulation type if nbody (e.g. 'Mill1')
     galformdir  = galform output path
        out_list = list of the values of the user specified parameters

        Caution: Beware of parameter names that are repeated as part of other parameter names, e.g. vhot and vhot_burst'''

    if tree.lower() == "nbody":
        iz = z2snap(zout,sim)
        output_dir = galformdir + "Nbody/" + sim + "/" + model + "/iz" + str(iz) + "/ivol"
        if violeta==True:
            output_dir = galformdir + sim + "/" + model + "/iz" + str(iz) + "/ivol"
        elif violeta=="will":
            output_dir = galformdir + sim + "/" + model + "/iz" + str(iz) + "/ivol"
        if "Track_Halos" in output_dir:
            galformdir_edit = galformdir[0:-11]
            output_dir = galformdir_edit + "Nbody/" + sim + "/" + model + "/Track_Halos/izfinal"+str(iz)+"/ivol"
        if sim == "Mill1" and tree_version == "old":
            output_dir += "3"
        elif sim == "MillGas" or tree_version == "new":
            output_dir += "0"
        else:
            print "Error: sim = "+ sim +" either does not exist or has not yet been implemented"
            quit()
    elif tree.lower() == "mctree":
        output_dir = galformdir + "MCtree/" + model + "/z" + str(zout)
        if "Track_Halos" in output_dir:
            galformdir_edit = galformdir[0:-11]
            if int(zout) == zout:
                output_dir = galformdir_edit + "MCtree/" + model + "/Track_Halos/zfinal"+str(int(zout))
            else:
                output_dir = galformdir_edit + "MCtree/" + model + "/Track_Halos/zfinal"+str(zout)
    else:
        print "Error: tree = "+tree+" type not recognised! Valid options are 'mctree' or 'nbody'"
        quit()

    out_list = []
    parameter_counter = 0

    ifile = output_dir + "/used-parameters"
    
    for parameter_name in parameter_list:
        parameter_file = open(ifile,'r')
        for line in parameter_file:
            if line.find(parameter_name) != -1:
                out_list.append(float(line.split()[2]))

        if len(out_list) == parameter_counter:
            print  "Error, parameter " + parameter_name + " was not found in the used-parameters file"
            quit()
        parameter_counter += 1

        parameter_file.close()

    return out_list

def read_galform_sfh(model,tree,sim,galformdir,zout,ident,jm,jtree=1,ivol=None,fine_grid=False,n_steps=None,tlb_fine=None,z_fine=None):
    '''Read ascii files for a singe galform galaxy sfh
    Usage: model = model name
           tree  = tree_type (e.g. 'mctree')
           sim   = simulation type if nbody (e.g. 'Mill1')
     galformdir  = galform output path
           zout  = output redshift
          ident  = galaxy id
             jm  = halo mass level id
          jtree  = halo id within a given mass level (always = 1 for Nbody or MCtree with continous halo mass sampling)
            ivol = subvolume id (for Nbody trees)
       fine_grid = if True: use linear interpolation for disk/bulge sfrs to refine tstep grid to n_steps steps
       n_steps  = see above
       tlb_fine  = This is NOT the new time step grid (I should change the names). 
       It is used to calculate the new z grid after you define the new time grid internally within this function.
       z_fine   = The corresponding redshift grid to tlb_fine
              z  = redshift
              t  = time (since big bang) /Gyr
            t_lb = look-back time / Gyr
      sfr_bulge  = Star formation rate in the bulge / (Msun /h /Gyr) 
      (note this is really star-formation that occured quiescently in progenitor disks before they merge with the final galaxy.
      This includes stars that formed in the main stellar progenitor disk before a major merger/disk instability occured.)
       sfr_disk  = Star formation rate in the disk  / (Msun /h /Gyr)
      sfr_burst  = Star formation rate in starbursts / (Msun /h /Gyr)'''

    if tree.lower() == "nbody":
        iz = z2snap(zout,sim)
        if ivol is None:
            print "Error: The subvolume ivol must be specified to read the sfh of a galaxy when using Nbody trees"
            quit()
        if sim == "Mill1":
            output_dir = galformdir + "Nbody/" + sim + "/" + model + "/iz" + str(iz) + "/ivol" + str(ivol) + "/SFH_Output/halo." + str(jm)
        elif sim == "MillGas":
            output_dir = galformdir + "Nbody/" + sim + "/" + model + "/iz" + str(iz) + "/ivol" + str(ivol) + "/SFH_Output/halo." + str(jm)
        else:
            print "Error: sim = "+ sim +" either does not exist or has not yet been implemented"
            quit()
    elif tree.lower() == "mctree":
        output_dir = galformdir + "MCtree/" + model + "/z" + str(zout) + "/SFH_Output/halo." + str(jm)
    else:
        print "Error: tree = "+tree+" type not recognised! Valid options are 'mctree' or 'nbody'"
        quit()
    
    ifile_disk = output_dir + "/sfr_disk." + str(ident)
    ifile_bulge = output_dir + "/sfr_bulge." + str(ident)
    ifile_burst = output_dir + "/bursts_bulge." + str(ident)

    z, t, t_lb, sfr_bulge = np.loadtxt(ifile_bulge,usecols=(0,1,2,4),unpack=True)
    sfr_disk = np.loadtxt(ifile_disk,usecols=(4,),unpack=True)

    if fine_grid:
        
        # Calculate correct final stellar masses so that we can renormalise to give the correct answer later.

        mass_disk_correct = np.sum(sfr_disk[1:] * (t[1:] - t[:-1]))
        mass_bulge_correct = np.sum(sfr_bulge[1:] * (t[1:] - t[:-1]))   

        # Get the desired new time grid with n_steps timesteps

        t_step = ((t.max()-t.min())/float(n_steps))
        t_fine = np.arange(t.min(),t.max(),t_step)
        if len(t_fine) == n_steps:
            t_fine = np.append(t_fine,np.max(t))
        
        # Fix a problem I was running into with interpolation endpoints in the SFR calculations
        if t_fine[-1] != t[-1]:
            t_fine[-1] = t[-1]

        sfr_disk = Linear_Interpolation(t,sfr_disk,t_fine)
        sfr_bulge = Linear_Interpolation(t,sfr_bulge,t_fine)

        t = t_fine

        t_step2 = ((t_lb.max()-t_lb.min())/float(n_steps))
        t_lb = np.arange(t_lb.min(),t_lb.max(),t_step2) #[0:n_steps][::-1]
        if len(t_lb) == n_steps:
            t_lb = np.append(t_lb,t_lb.max()+t_step2)
        t_lb = t_lb[::-1]

        # Renormalise such that integration still gives correct final stellar mass

        mass_disk_interp = np.sum(sfr_disk[1:] * (t[1:] - t[:-1]))
        mass_bulge_interp = np.sum(sfr_bulge[1:] * (t[1:] - t[:-1]))

        if mass_disk_correct == 0.0:
            renorm_disk = 0.0
        else:
            renorm_disk = mass_disk_correct / mass_disk_interp
        if mass_bulge_correct == 0.0:
            renorm_bulge = 0.0
        else:
            renorm_bulge = mass_bulge_correct / mass_bulge_interp

        sfr_disk *= renorm_disk
        sfr_bulge *= renorm_bulge

        # Calculate the corresponding z grid by using interpolation in time

        z = Linear_Interpolation(tlb_fine,z_fine,t_lb)

    #Check to see if the galaxy underwent any bursts
    try:
        z_bursts, t_bursts, t_lb_bursts, mgas0_bursts, taue_bursts, tdur_bursts, R_Beta_Factor, mass_bursts_correct = np.loadtxt(ifile_burst,usecols=(0,1,2,3,4,5,8,13),unpack=True)

        sfr_bursts = np.zeros_like(t_lb)

        try:
            for n in range(len(z_bursts)):
                sfr_burst = mgas0_bursts[n] / R_Beta_Factor[n] /taue_bursts[n] * np.exp(-(t-t_bursts[n])/taue_bursts[n])

                outside_burst = (t<t_bursts[n]) | (t>t_bursts[n]+tdur_bursts[n])
                sfr_burst[outside_burst] = 0.

                mass_burst_interp = np.sum(sfr_burst[1:] * (t[1:] - t[:-1]))
                
                # Deal with cases where burst timescale is smaller than the temporal resolution of the grid.
                if mass_burst_interp == 0.0:
                    closest_inds = np.sort(np.argsort(abs(t_bursts[n]-t))[0:2])
                    timestep = t[closest_inds[1]] - t[closest_inds[0]]
                    sfr_burst[closest_inds[1]] = mgas0_bursts[n] / R_Beta_Factor[n] / timestep

                    mass_burst_interp = np.sum(sfr_burst[1:] * (t[1:] - t[:-1]))

                # Renormalise so that the correct mass is formed in bursts when integrated.

                renorm_burst = mass_bursts_correct[n] / mass_burst_interp

                sfr_burst *= renorm_burst

                sfr_bursts += sfr_burst

        except TypeError:
            sfr_bursts = mgas0_bursts / R_Beta_Factor /taue_bursts * np.exp(-(t-t_bursts)/taue_bursts)
            outside_burst = (t<t_bursts) | (t>t_bursts+tdur_bursts)
            sfr_bursts[outside_burst] = 0.

            mass_burst_interp = np.sum(sfr_bursts[1:] * (t[1:] - t[:-1]))

            if mass_burst_interp == 0.0:
                    print "Error, one of the bursts is occuring below your temporal resolution. Increase the number of timesteps"
                    quit()
            
            renorm_burst = mass_bursts_correct / mass_burst_interp

            sfr_bursts *= renorm_burst

    except ValueError:
        sfr_bursts = np.zeros_like(t_lb)

    return z, t, t_lb, sfr_bulge, sfr_disk, sfr_bursts

def Large_Subvolume_List():
    subvolumes = [2, 4, 5, 6, 7, 8, 15, 36, 40, 44, 45, 46, 60, 68, 76, 83, 90, 97, 98, 101, 119, 121, 123, 125, 130, 137, 145,
                  149, 150, 163, 173, 178, 184, 185, 191, 196, 197, 199, 216, 217, 225, 226, 237, 249, 271, 272, 278, 279, 280,
                  300, 301, 305, 316, 325, 334, 341, 347, 356, 358, 359, 365, 375, 380, 383, 386, 390, 405, 406, 407, 417, 435,
                  443, 447, 460, 475, 480, 486, 488, 491, 492, 498]
    return subvolumes

def SFG_Cut(model,z):
    '''calculate the gradient/intercept of a power law that seperates
    star forming and passive galaxies based on their distibrution in
    specific star foramtion rate vrs stellar mass.
    
    returns m,c corresponding to log(ssfr/yr) = m * (logm - 10) + c'''

    z_cut = np.array([ 0.0 ,  0.1 ,   0.2 ,   0.4 ,   0.5 ,  0.7 ,  0.8 ,  0.9 ,  1.0 ,  1.4,  1.5,  1.8 ,  2.0 ,  2.2 ,  2.4 ,  2.8 ,  3.0 ,  3.6 ,  3.9,  4.2,  4.9,  5.3,  6.2, 11.2, 12.0])
    if model[0:6] == "Lagos1" or model[0:3] == "Toy" or model[0:8] == "Fiducial" or "BetaToy" or "Gonzalez14" in model:
        m = np.array([     0.0 ,  0.0 ,   0.0 ,   0.0 ,   0.0 ,  0.0 ,  0.0 ,  0.0 ,  0.0 ,  0.0,  0.0, -0.2 , -0.2 , -0.2 , -0.2 , -0.2 , -0.2 , -0.2 , -0.2, -0.2, -0.2, -0.2, -0.2, -0.2, -0.2])
        c = np.array([    -11.4, -11.2,  -11.0,  -10.8,  -10.6, -10.3, -10.2, -10.0, -10.0, -9.8, -9.8, -10.0, -10.0, -10.0, -10.0, -10.0, -10.0, -10.0, -9.8, -9.8, -9.8, -9.8, -9.8, -9.8, -9.8])
    elif model == "Uber_MillGas_Latest3":
        m = np.array([     0.0 ,  0.0 ,   0.0 ,   0.0 ,   0.0 ,  0.0 ,  0.0 ,  0.0 ,  0.0 ,  0.0,  0.0, -0.1, -0.1, -0.1, -0.1, -0.1, -0.1, -0.1, -0.1, -0.1, -0.1, -0.1, -0.1,-0.1, -0.1])
        c = np.array([    -10.9, -10.7,  -10.6,  -10.5,  -10.3, -10.3, -10.2, -10.0, -10.0, -9.8, -9.8, -9.8, -9.7, -9.7, -9.7, -9.6, -9.6, -9.6, -9.5, -9.4, -9.4, -9.4, -9.4,-9.4, -9.4])
    elif model == "Bow06":
        m = np.array([     0.0 ,  0.0 ,   0.0 ,   0.0 ,   0.0 ,  0.0 ,  0.0 ,  0.0 ,  0.0 ,  0.0,  0.0, -0.1 , -0.1 , -0.1 , -0.1 , -0.1 , -0.1 , -0.1 , -0.1, -0.1, -0.1, -0.1, -0.1,-0.1, -0.1])
        c = np.array([    -10.9, -10.8,  -10.6,  -10.5,  -10.3, -10.3, -10.2, -10.0, -10.0, -9.8, -9.8, -9.7, -9.7, -9.6, -9.5, -9.4, -9.4, -9.4, -9.4, -9.4, -9.4, -9.4, -9.4,-9.4,-9.4])
    elif model == "Bau05":
        m = np.array([     0.0 ,  0.0 ,   0.0 ,   0.0 ,   0.0 ,  0.0 ,  0.0 ,  0.0 ,  0.0 ,  0.0,  0.0, -0.1 , -0.1 , -0.1 , -0.1 , -0.1 , -0.1 , -0.1 , -0.1, -0.1, -0.1, -0.1, -0.1,-0.1,-0.1])
        c = np.array([    -15.5, -15.5,  -15.5,  -15.5,  -15.5, -15.5, -15.5, -15.5, -15.5, -15.5, -15.5, -15.5, -15.5, -15.5, -15.5, -15.5, -15.5, -15.5, -15.5, -15., -15., -15., -15.5,-15.5,-15.5])
    else:
        print "Error: model = ", model, " does not have a star-forming/passive ssfr-m cut yet"
        quit()

    #Linearly interpolate in redshift (should really be in lookback time but nevermind
    m_out = Linear_Interpolation(z_cut,m,[z])
    c_out = Linear_Interpolation(z_cut,c,[z])

    return m_out, c_out

def Z_to_Z_FeH(Z):
    ''' input is metallicity Z, NOT in units of Z_sun!'''
    Z_FeH = Z * 56.39 / (1. - 4.474*Z)
    return Z_FeH

def X_from_Z_FeH(Z_FeH):
    ''' calculate hydrogen mass fraction of a gas of metallicity Z_FeH'''
    Z_Metals_Solar=0.0189
    Z_Metals  =5.36e-10
    X_Hydrogen_Solar=0.707
    X_Hydrogen=0.778
    Z_FeH_Max=3.16227766

    dXdZ_FeH=(X_Hydrogen_Solar-X_Hydrogen)/(Z_to_Z_FeH(Z_Metals_Solar)-Z_to_Z_FeH(Z_Metals))

    X_Hydrogen_of_Z_Metals=X_Hydrogen+dXdZ_FeH*np.minimum(Z_FeH,np.zeros_like(Z_FeH)+Z_FeH_Max)

    return X_Hydrogen_of_Z_Metals

def Calculate_Cooling_Function(T,Zhot):
    ''' Use interpolation to get the cooling function, Lambda(T,Z) in J s^-1 m^3
    Cooling functions are read in from tables originally described in
    Sutherland & Dopita (1993). The cooling function is normalised to unit density
    of hydrogen in cm^3.

    DO NOT feed any NaNs into this routine, it will crash!'''

    path = "/cosma/home/d72fqv/Galform-2.5.3/Data/cooling/"
    file_list = ["mzero.cie",
                 "m-30.cie",
                 "m-20.cie",
                 "m-15.cie",
                 "m-10.cie",
                 "m-05.cie",
                 "msol.cie",
                 "m+05.cie"]

    # These metallicities are in units of 10^(Fe/H) instead of usual convention
    Z_FeH_grid = np.array([0.0, 0.001, 0.01, 0.0316228, 0.1, 0.316228, 1.0, 3.16228])
    nZ = len(Z_FeH_grid)

    logT_grid = np.loadtxt(path+file_list[0],unpack=True,usecols=[0])
    n_T = len(logT_grid)

    T_grid = 10.**logT_grid

    if T.max > T_grid.max or T.min < T_grid.min:
        if T.max() > T_grid.max():
            print "Temperature out of bounds (too high) from Sutherland & Dopita grid, setting to endpoint value"
            ok = T < T_grid.max()
            T[ok==False] = T_grid.max()

        if T.min() < T_grid.min():
            print "Temperature out of bounds (too low) from Sutherland & Dopita grid, setting to endpoint value"
            ok = T > T_grid.min()
            T[ok==False] = T_grid.min()

    # Define log(Lambda) grid

    logLam_grid = np.zeros(( len(Z_FeH_grid), len(logT_grid) ))

    for n in range(nZ):
        logLam_grid[n] = np.loadtxt(path+file_list[n],unpack=True,usecols=[4])

    # Run interpolation in log(Lambda), log(T) and Z to get log(Lambda) for desired Z,T

    Z_FeH = Z_to_Z_FeH(Zhot)
    
    okZ = Z_FeH <= Z_FeH_grid.max()
    if len(Z_FeH[okZ])<len(Z_FeH):
        #print "Metallicity (at least one of them) is greater than the maximum value in the Sutherland & Dopita grid. Setting to endpoint metallicity"
        Z_FeH[okZ==False] = Z_FeH_grid.max()

    logT_desired = np.log10(T)

    # 1st perform interpolation in log(T)

    logLam_broadcast = np.swapaxes(logLam_grid,0,1)

    logLam_interpT = Linear_Interpolation(logT_grid,logLam_broadcast,logT_desired)

    logLam_broadcast = np.swapaxes(logLam_interpT,0,1)

    # 2nd, perform interpolation in Z for these values

    ind_Z = np.argmin(abs(np.reshape(Z_FeH,(len(Z_FeH),1))-Z_FeH_grid),axis=1)
    ind_Z_1 = np.copy(ind_Z)
    ind_Z_2 = np.copy(ind_Z_1) +1

    gt = Z_FeH > Z_FeH_grid[ind_Z_1]
    ind_Z_2[gt == False] -= 1
    ind_Z_1[gt == False] -= 1

    ok = (ind_Z_2 < len(Z_FeH_grid)) & (ind_Z_1 >= 0)

    if len(ind_Z_1[ok==False]) != 0:
        print np.sort(Z_FeH)
        print np.sort(Zhot)
        print len(ind_Z_1[ok==False]), len(ind_Z_1)
        print "code can't deal with metallicities outside the sutherland & dopita grid at present, crashing now"
        quit()

    Z_1 = Z_FeH_grid[ind_Z_1[ok]]; Z_2 = Z_FeH_grid[ind_Z_2[ok]]

    fraction = (Z_2-Z_FeH[ok])/(Z_2-Z_1)

    logLam_out = np.zeros_like(Z_FeH)

    logLam_1 = np.choose(ind_Z_1[ok],logLam_broadcast)
    logLam_2 = np.choose(ind_Z_2[ok],logLam_broadcast)

    logLam_out[ok] = logLam_1 * fraction + logLam_2 * (1-fraction)
    #logLam_out[ok==False] = np.diagonal(logLam_broadcast[ind_Z[ok==False],:])

    # Below line should in principle be the slow version of the above.
    #print "complete, now performing 2nd interpolation using old method"
    #logLam_out = np.diagonal(Linear_Interpolation(Z_FeH_grid,logLam_broadcast,Z_FeH)) # log_10 (erg s^-1 cm^3)

    Lambda_cool = 10.**logLam_out # erg s-1 cm^3

    Lambda_cool *= 10**-7 # J s^-1 cm^3 = kg m^2 cm^3 s^-3
    Lambda_cool *= 10**-6 #J s^-1 m^3 = kg s^-3 m^5

    return Lambda_cool

def Calculate_Cooling_Time(M_hot, R_h, V_h, Zhot):
    ''' Calculate the characteristic timescale for a halo of hot gas to cool in Gyr,
    assuming a constant density, isothermal density profile. The cooling function
    is taken from Sutherland & Doptita (1993).
    Inputs:
       M_h = Mass of hot gas inside the halo in Msun
       R_h = Virial radius in kpc
       V_h = Circular velocity at the virial radius in km s^-1
       Zhot = Metallicity of the hot gas, in standard units (not in units of Z_sun)
    Ouput:
       t_cool = cooling timescale in Gyr'''

    # constants
    kb = 1.3806488 * 10**-23 # m^2 kg s^-2 K^-1
    M_Atomic=1.66053873e-27 # kg
    Atomic_Mass_Hydrogen=1.00794 # in units of M_Atomic
    Atomic_Mass_Helium=4.002602 # in units of M_Atomic
    fH = 0.778 # primordial hyrdrogen abundance
    fHe = 0.222 # primordial helium abundance
    Msun = 1.98855 * 10**30 #kg
    pc = 3.08567758 * 10**16 # m
    kpc = pc * 10**3
    Gyr = 365.25 * 24 * 3600 * 10**9 # s
    kms = 10**3 # ms^-1

    # Convert metallicity to units of Fe/H
    Z_FeH = Z_to_Z_FeH(Zhot)

    # Calculate hydrogen mass fraction
    X = X_from_Z_FeH(Z_FeH)

    # Calculate mean molecular weight in atomic mass units of a primordial gas
    mu_Primordial=1.0/(2.0*fH/Atomic_Mass_Hydrogen+3.0*fHe/Atomic_Mass_Helium) # Mean atomic weight

    # For an isothermal profile, the temperature of the gas is approximated by the Virial temperature  
    T = 0.5 * mu_Primordial * M_Atomic / kb * (V_h*kms)**2 # Kelvin

    # Get the cooling function

    Lambda_cool = np.zeros_like(T)
    ok = np.isnan(Zhot) == False
    
    Lambda_cool[ok == False] = np.nan
    if len(T[ok]) > 0:
        Lambda_cool[ok] = Calculate_Cooling_Function(T[ok],Zhot[ok]) # J s^-1 m^3

    # Calculate the cooling luminosity per unit mass
    L_cool = 3/(4*np.pi) * X**2 * Lambda_cool * M_hot * Msun / (R_h*kpc)**3 / (Atomic_Mass_Hydrogen * M_Atomic)**2 # J s^-1 kg^-1

    # Calculate the internal energy per unit mass
    U = 1.5 * kb * T / (mu_Primordial * M_Atomic) # m^2 s^-2 = J kg^-1

    # Calculate the cooling timescale
    t_cool = U/L_cool/Gyr

    # Fudge factor obtained by comparison to the internal galform calculation for a few halo masses. 02/12/2013
    # Use this factor if you want cooling timescale for gas with correct local density at the virial radius (provided rcore<<rvir)
    # Otherwise t_cool corresponds to the cooling timescale associated with the mean halo density within the virial radius
    #fudge_factor = 3.0
    #t_cool *= fudge_factor 
    
    return t_cool

def Calculate_Stellar_Mass_Completeness(mstar,mhalo,weights,mhalo_min=10.5,completeness=0.9,bin_width=0.2):
    """Calculate the stellar mass down to which you are "completeness" complete,
    assuming that you complete down to a halo mass of mhalo_min.
    
    Inputs: mstar = stellar masses of galaxy sample in Msun
            mhalo = halo masses of galaxy sample in Msun
            weights = relative weight of each galaxy in units of number density
            mhalo_min = log_10(minimum halo mass / Msun) for which your simulation is complete
            completeness = fraction of galaxies required to be regarded complete
            bin_width = width of mhalo bins in dex

    Outputs: Stellar mass as which you are "completeness" complete in Msun"""

    from utilities_statistics import Weighted_Percentile

    bin = np.array([mhalo_min,mhalo_min+bin_width])

    index_bin = (np.log10(mhalo)<bin[1]) & (np.log10(mhalo)>bin[0])

    mstar_complete = Weighted_Percentile(mstar[index_bin],weights[index_bin],completeness)

    return mstar_complete

def Calculate_Magnitude_Completeness(mag,mhalo,weights,mhalo_min=10.5,completeness=0.9,bin_width=0.2):
    """Calculate the magnitude down to which you are "completeness" complete,
    assuming that you complete down to a halo mass of mhalo_min.
    
    Inputs: mag = magnitdes of galaxies
            mhalo = halo masses of galaxy sample in Msun
            weights = relative weight of each galaxy in units of number density
            mhalo_min = log_10(minimum halo mass / Msun) for which your simulation is complete
            completeness = fraction of galaxies required to be regarded complete
            bin_width = width of mhalo bins in dex

    Outputs: Magnitude down to which you are "completeness" complete in Msun"""

    from utilities_statistics import Weighted_Percentile

    bin = np.array([mhalo_min,mhalo_min+bin_width])

    index_bin = (np.log10(mhalo)<bin[1]) & (np.log10(mhalo)>bin[0])

    mag_complete = Weighted_Percentile(mag[index_bin],weights[index_bin],1.0-completeness)

    return mag_complete

if __name__=='__main__':

    # Calculate lambda cool for comparison with the textbook answer.

    Lambda = Calculate_Cooling_Function(np.array([10.**5]),np.array([0.0189]))

    t_cool =  Calculate_Cooling_Time(np.array([1]), np.array([1]), np.array([1]), np.array([0.0189]))
    print np.log10(t_cool * 10**9 * 365.25 * 24 * 3600)
    exit()

    print Lambda[0] * 10**7 * 10**23 * 10**6 , ' 10^-23 erg s^-1 cm^3 '
    exit()

    #Check nearest snapshot iz for a given redshift
    z = 0.5
    iz = z2snap(z,'MillGas')
    print iz
    quit()

    # Example for reading Nbody output
    model = "Lagos12"
    trees = "Nbody"
    z=2
    sim = "Mill1"
    path = "/gpfs/data/d72fqv/Galform_output/"
    subvols = [3,31,47,229,231,266,334,381,424]
    data = read_galform_subvolumes(galformdir=path,model=model,zout=z,sample_file=None,subvolumes=subvols,sim=sim,props=None,galform_type='nbody',verbose=True)
    print data['weight']
    print len(data['weight'])
    exit()

    '''# Example for reading MCtree output
    model = "Bau05"
    trees = "mctree"
    z = 2
    sim = None
    path = "/gpfs/data/d72fqv/Galform_output/"
    subvols = None
    data = read_galform_subvolumes(path,model,z,"Processed_Gals.hdf5",subvols,sim,None,trees,True)
    print data['weight']'''

    model = "Bau05"
    trees = "mctree"
    z = 2
    sim = None
    path = "/gpfs/data/d72fqv/Galform_output/"
    print get_recycled_fraction_yield(model,trees,sim,path,z)

