import numpy as np
from utilities_galform import *
from scipy import integrate
from utilities_sed import Print_Statements, Calzetti_Attenuation



############### Choose parameter file ########################################################

from fit_parameters_default import *
#print "Using taylor 11 fitting parameters"
#from fit_parameters_taylor11 import *
#print "Using SDSS parameters"
#from fit_parameters_sdss import *
#print "Using Vypers parameters"
#from fit_parameters_vypers import *
#print "Using Will's requested parameters"
#from fit_parameters_will import *


################ Choose galform model(s) #########################################################

model_list = ["Lagos12"] # model name
tree_list=["nbody"] # Tree type: either "nbody" or montecarlo "mctree"
sim_list=["Mill1"] # Simulation list for models run on nbody; e.g. "Mill1", "Mill2", "MillGas". Use None for mctree

# specify subvolumes if using nbody trees; irrelevant if using mctree
subvolumes = [3 , 31, 47, 229, 231, 266, 334, 381, 424] # Standard list for sim = "Mill1"
#subvolumes = [0] # Standard list for sim = "MillGas"


############### SED fitting options ############################################################

# Note: turning the wrong combination of these on/off will cause conflicts!

DustFit = True # True: SED fitting treats E(B-V) as a free parameter. False: SED fitting constrains E(B-V) = 0
DustGalform = True # True: model galaxy magnitudes from Galform are attenuated by dust. False: No dust correction in Galform
UseCalzetti = False # True: Use a Calzetti law instead of Cedric's dust model to attenuate Galform galaxies. NOT recommended.

One_Metallicity = False # True: Constrain metallicity to a single value in the SED fitting. False: Metallicity is a free parameter in the fitting.
Metallicity_ind = 1 # Index in the metallicity grid corresponding to the constrained metallicity if One_Metallicity = True
Santini_Limit = False # True: Prevent super-solar metallicities in the SED fitting for z>1
Extra_Metallicity = False # True: Allow SED fitting to use any additional metallicities that have been included in the template grid via interpolation.
ForceCorrectZ = False # True: Force SED fitting to choose metallicity that is closest to the mass weighted metallicity of galaxy in Galform.

One_Tau = False # Equivalent to One_Metallicity but for the e-folding timescale in the template SFHs
Tau_ind = 11

Rest_Frame = False # Do SED fitting with filters evaluated in the galaxy rest-frame instead of redshifted to the observer frame. Requires rest-frame templates.
No_IRAC = False # Remove all filters with effective wavelength in the observer frame > 25000 Ang from the SED fitting.
No_NIR = False # Remove all filters with effective wavelength in the observer frame > 10000 Ang from the SED fitting.



################# Output options ###################################################################


Output_Mode = "Default" # "Default" = output everything to /gpfs/data/d72fqv/SED_Fitting/Fitted_Data/....... as .npy files
# If Output_Mode is set = to anything else, output is written to the directory where the Galform output is stored.

#This option is only used if Output_Mode is not "Default"
output_format = "default" # "default" ==> Output is written as .npy files, "ascii" ==> Output is written as plain text files.
WriteChiSquared = False # Output the best-fit chi-squared for each galaxy
WriteEBV = False # Output the best-fit E(B-V) colour excess for each galaxy
Write_s = False # Output the best-fit normalisation factor for each galaxy
Write_SFR = False # Output the best-fit SFR for each galaxy
Instantaneous_Recycling = False # Also output the stellar mass that would be obtained if the instantaneous recyling approximation is used in the SED fitting.
Weighted_Average = False # Also output the stellar mass obtained by taking the mean over the proability distribution function calculated by the SED fitting.

# Change output path for certain options
if Rest_Frame:
    SPS += '/RestFrame'
if Dust_Law == 'SlabCalzetti':
    SPS += '/SlabCalzetti'

################# Input paths for galform files ####################################################

galform_path = "/gpfs/data/d72fqv/Galform_output/v2.5.3/"
#galform_path = "/gpfs/data/rvjl73/Galform_Output/v2.5.3/"

filename = "Processed_Gals.hdf5"
#filename = "o2.hdf5"
#filename = "main.hdf5"
#filename = "Ldust.hdf5"

if Rest_Frame:
    frame_str = 'r'
else:
    frame_str = 'o'

props = ["mstars_total","mstars_disk","mstars_bulge","metSM_total"]
#props = ["mstars_disk","mstars_bulge","metSM_total","mstars_total"]
#props = ["mstars_tot"]
for band in band_list:
    if DustGalform and not UseCalzetti:
        props.append("mag"+band+frame_str+"_tot_ext")
    else:
        props.append("mag"+band+frame_str+"_tot")



################ Print SED fitting/output options to standard out #######################################

Print_Statements(DustFit,DustGalform,UseCalzetti,One_Metallicity,Metallicity_ind,One_Tau,Tau_ind,Santini_Limit,Extra_Metallicity,ForceCorrectZ,Instantaneous_Recycling,template_Z_grid,tau_grid,SFH_Type, Rest_Frame, Weighted_Average, Dust_Law,UseCalzetti)






################ Model loop ###########################################################################

lambda_eff_save = np.copy(lambda_eff) # save this array in case it gets changed later

for model,tree,sim in zip(model_list,tree_list,sim_list):
    
    # Read cosmological parameters used for this model
    h,omm,oml,omb = get_cosmological_parameters(model,tree,sim,galform_path,z_list[0],violeta="will")
    # Read recyled fraction to be used if Instantaneous_Recycling = True
    R, yld = get_recycled_fraction_yield(model,tree,sim,galform_path,z_list[0],violeta="will")

    
    ##################### Redshift Loop ################################################################

    for i in range(len(z_array)):


        ################## Subvolume loop ##############################################################
        # Note this is not looped over if Output_Mode = "Default"

        for j in range(len(subvolumes)):

            if Output_Mode == "Default": # Load all subvolumes and output as one file to a separate directory
                subvolume = subvolumes
                if j > 0:
                    break
            else: # Run SED fitting on each subvolume individually and output files to each subvolume directory
                subvolume = [subvolumes[j]]

            z = z_array[i]
            if z == int(z):
                z = int(z)
            print 'Fitting '+model+ ' at redshift z = ',z

            lambda_eff = lambda_eff_save # Reset effective wavelength array back to values given in parameter file





            ################  Read Galform data ##########################################################

            
            galform_data = read_galform_subvolumes(galform_path,model,z,filename,subvolume,sim,props,tree,verbose=True,violeta="will")

            try:
                #galform_mass = galform_data["mstars_total"] / h # Total stellar mass
                galform_mass = galform_data["mstars_tot"] / h
            except:
                mstars_disk = galform_data["mstars_disk"] / h
                mstars_bulge = galform_data["mstars_bulge"] / h
                galform_mass = mstars_disk + mstars_bulge # Total stellar mass
            ngal = len(galform_mass)

            try:
                galform_mwZ = get_sample_gals_property("metSM_total") # Mass weighted metallicity
            except:
                if not ForceCorrectZ:
                    print "Error: couldn't read mass weighted metallcity from galform output file"
                    print "This was required for option ForceCorrectZ=True"
                    print "Either change to ForceCorrectZ = False or change the galform output file"
                    quit()

            n_bands = len(band_list)
            galform_mag_grid = np.zeros((n_bands,ngal)) #Grid of galform AB absolute magnitudes. 1)Band 2)Galaxy

            for n in range(n_bands):
                galform_mag_grid[n] = galform_data[props[len(props)-n_bands+n]] + 5*np.log10(h)

            if UseCalzetti: #Use the Calzetti law instead of Cedric's dust model to attenuate Galform magnitudes
                mstardot_average = get_sample_gals_property("mstardot_average") /h/10.0**9.0
                mstardot_burst = get_sample_gals_property("mstardot_burst")/h/10.0**9.0
                EBV_gal = 0.33*(np.log10(mstardot_average+mstardot_burst)-2)+1.0/3.0
                EBV_gal[mstardot_average+mstardot_burst < 10.0] = 0.0

                if not Rest_Frame:
                    attenuation_factors = Calzetti_Attenuation(lambda_eff/(1.0+z),EBV_gal)
                else:
                    attenuation_factors = Calzetti_Attenuation(lambda_eff,EBV_gal)
                dmag = -2.5*np.log10(attenuation_factors)
                dmag = np.swapaxes(dmag,0,1)
                galform_mag_grid += dmag

            

            ###########   Read Template grid for SED fitting ####################################################

            #Galaxy age grid (Gyr)
            template_age_grid = np.load('/gpfs/data/d72fqv/SED_Fitting/Template_Data/'+SFH_Type+'/'+SPS+'/z'+str(z)+'_age_grid.npy')

            #AB absolute magnitude grid # 1)Band 2)Attenuation 3) Metallicity 4) Age 5) Tau
            template_mag_grid = np.load('/gpfs/data/d72fqv/SED_Fitting/Template_Data/'+SFH_Type+'/'+SPS+'/z'+str(z)+'_magAB_grid.npy')

            #Stellar mass (Msun) grid with 1)metallicity 2) age 3) tau
            template_mass_grid = np.load('/gpfs/data/d72fqv/SED_Fitting/Template_Data/'+SFH_Type+'/'+SPS+'/z'+str(z)+'_stellar_mass_grid.npy')

            #Stellar remnant mass (Msun) grid with 1)metallicity 2) age 3) tau
            template_mass_rem_grid = np.load('/gpfs/data/d72fqv/SED_Fitting/Template_Data/'+SFH_Type+'/'+SPS+'/z'+str(z)+'_remnant_mass_grid.npy')
            # Note stellar mass include remnant mass.

            #Mass weighted age (Gyr) grid with 1)metallicity 2) age 3) tau
            template_mass_weighted_age_grid = np.load('/gpfs/data/d72fqv/SED_Fitting/Template_Data/'+SFH_Type+'/'+SPS+'/z'+str(z)+'_mass_weighted_age_grid.npy')

            # Stellar mass for templates if there is no recycling from stellar evolution. Used later if Instantaneous_Recyling = True
            if Instantaneous_Recycling:
                template_mass_ira_grid = np.load('/gpfs/data/d72fqv/SED_Fitting/Template_Data/'+SFH_Type+'/'+SPS+'/z'+str(z)+'_mass_ira_grid.npy')

            #Convert all magnitudes to relative fluxes
            galform_flux_grid  = 10**(-1*(galform_mag_grid)/2.5)
            template_flux_grid = 10**(-1*template_mag_grid/2.5)

            #Errors used in Chi-Squared calculation
            sigma_flux = galform_flux_grid * 0.1



            ############# Trim down template grid to account for different SED fitting options #######################################


            #Change arrays for the no dust, no extra metallicity and 1 metallicity cases
            if DustFit == False:
                template_flux_grid = np.delete(template_flux_grid,np.arange(1,len(EBV_grid)),1)

            use_template_Z_grid = template_Z_grid
            use_tau_grid = tau_grid

            if Extra_Metallicity == False and len(use_template_Z_grid) != len(sps_files):
                select = np.arange(len(sps_files),len(use_template_Z_grid))
                use_template_Z_grid = np.delete(use_template_Z_grid,select)
                template_flux_grid = np.delete(template_flux_grid,select,2)
                template_mass_grid = np.delete(template_mass_grid,select,0)
                if Instantaneous_Recycling:
                    template_mass_ira_grid = np.delete(template_mass_ira_grid,select,0)
                template_mass_rem_grid = np.delete(template_mass_rem_grid,select,0)
                template_mass_weighted_age_grid = np.delete(template_mass_weighted_age_grid,select,0)

            if One_Metallicity:
                select = np.arange(0,len(use_template_Z_grid))
                select = select[select!=Metallicity_ind]
                use_template_Z_grid = np.delete(use_template_Z_grid,select)
                template_flux_grid = np.delete(template_flux_grid,select,2)
                template_mass_grid = np.delete(template_mass_grid,select,0)
                if Instantaneous_Recycling:
                    template_mass_ira_grid = np.delete(template_mass_ira_grid,select,0)
                template_mass_rem_grid = np.delete(template_mass_rem_grid,select,0)
                template_mass_weighted_age_grid = np.delete(template_mass_weighted_age_grid,select,0)

            if One_Tau:
                select = np.arange(0,len(use_tau_grid))
                select = select[select!=Tau_ind]
                use_tau_grid = np.delete(use_tau_grid, select)
                template_flux_grid = np.delete(template_flux_grid,select,4)
                template_mass_grid = np.delete(template_mass_grid,select,2)
                if Instantaneous_Recycling:
                    template_mass_ira_grid = np.delete(template_mass_ira_grid,select,2)
                template_mass_rem_grid = np.delete(template_mass_rem_grid,select,2)
                template_mass_weighted_age_grid = np.delete(template_mass_weighted_age_grid,select,2)

            if Santini_Limit and z > 1.0 and not One_Metallicity: #Remove Super-Solar metallicity grid points if z>1
                select = np.arange(0,len(use_template_Z_grid))
                select = select[use_template_Z_grid > 0.0201]
                use_template_Z_grid = np.delete(use_template_Z_grid,select)
                template_flux_grid = np.delete(template_flux_grid,select,2)
                template_mass_grid = np.delete(template_mass_grid,select,0)
                if Instantaneous_Recycling:
                    template_mass_ira_grid = np.delete(template_mass_ira_grid,select,0)
                template_mass_rem_grid = np.delete(template_mass_rem_grid,select,0)
                template_mass_weighted_age_grid = np.delete(template_mass_weighted_age_grid,select,0)

            if No_IRAC:
                print 'Spitzer IRAC NIR bands excluded from the SED fitting process'
                select_bands = np.arange(0,n_bands)
                select_bands = select_bands[lambda_eff > 25000]
                template_flux_grid = np.delete(template_flux_grid,select_bands,0)
                galform_flux_grid = np.delete(galform_flux_grid,select_bands,0)
                sigma_flux = np.delete(sigma_flux,select_bands,0)
                n_bands -= len(select_bands)
                lambda_eff_save = lambda_eff
                lambda_eff = np.delete(lambda_eff,select_bands)

            if No_NIR:
                print 'All NIR bands excluded from the SED fitting process'
                select_bands = np.arange(0,n_bands)
                select_bands = select_bands[lambda_eff > 10000]
                template_flux_grid = np.delete(template_flux_grid,select_bands,0)
                galform_flux_grid = np.delete(galform_flux_grid,select_bands,0)
                sigma_flux = np.delete(sigma_flux,select_bands,0)
                n_bands -= len(select_bands)
                lambda_eff_save = lambda_eff
                lambda_eff = np.delete(lambda_eff,select_bands)

            if not Rest_Frame:
                select_bands = np.arange(0,n_bands)
                select_bands = select_bands[lambda_eff/(1.0+z) < 912.0]
                template_flux_grid = np.delete(template_flux_grid,select_bands,0)
                galform_flux_grid = np.delete(galform_flux_grid,select_bands,0)
                sigma_flux = np.delete(sigma_flux,select_bands,0)
                lambda_eff = np.delete(lambda_eff,select_bands)
                n_bands -= len(select_bands)
                for m in range(len(select_bands)):
                    print band_list[select_bands[m]], ' band has been removed because it is beyond the Lyman limit at z=',z



            ############### Initialise output arrays #######################
            sed_stellar_mass = np.zeros(ngal) 
            sed_remnant_mass = np.zeros(ngal)
            sed_age = np.zeros(ngal)
            sed_tau = np.zeros(ngal)
            sed_Z = np.zeros(ngal)
            sed_mass_weighted_age = np.zeros(ngal)
            sed_chisquared = np.zeros(ngal)
            sed_EBV = np.zeros(ngal)
            sed_mass_ira = np.zeros(ngal)
            sed_s = np.zeros_like(sed_Z)
            sed_sfr = np.zeros_like(sed_Z)
            sed_weighted_average_mass = np.zeros(ngal)
            sed_weighted_average_mass_ira = np.zeros(ngal)
            sed_weighted_average_Z = np.zeros(ngal)


            print_ind = 0

            #################### Galaxy loop #################################################################
            for m in range(ngal):
                print_ind+=1
                if m == 0:
                    print "starting galaxy loop"
                if print_ind > ngal/20:
                    print m, ' of ',ngal
                    print_ind =0

                ### Compute normalisation, s, that minimises chi-squared for each template w.r.t galaxy SED ######

                s_top =np.zeros(template_flux_grid.shape[1:])#1)Attenuation 2)Metallicity 3)Age 4)Tau
                s_top = s_top.ravel()
                s_bottom = np.zeros_like(s_top)

                for n in range(n_bands):
                    s_top += template_flux_grid[n].ravel()*(galform_flux_grid[n,m]/np.square(sigma_flux[n,m]))
                    s_bottom += np.square(template_flux_grid[n].ravel()) *(1.0/np.square(sigma_flux[n,m]))

                s = s_top / s_bottom

                ### Calculate chi-squared grid 1)Attenuation 2)Metallicity 3)Age 4)Tau
                chi = np.zeros_like(s)

                for n in range(n_bands):
                    chi += np.square( (galform_flux_grid[n,m] - s*template_flux_grid[n].ravel() )*(1.0/(sigma_flux[n,m])))

                ### Calculate stellar mass if likelihood weighted summation over template grid is used instead of best-fit template
                if Weighted_Average:
                    full_template_mass_grid = np.zeros(template_flux_grid.shape[1:])
                    for n in range(len(template_flux_grid[0,:,0,0,0])):
                        full_template_mass_grid[n] = template_mass_grid
                    weighted_average_mass = np.sum(s*full_template_mass_grid.ravel()*np.exp(-chi/2.0)) / np.sum(np.exp(-chi/2.0))

                    full_template_Z_grid = np.reshape(use_template_Z_grid,(1,len(use_template_Z_grid),1,1))

                    weighted_average_Z = np.sum(full_template_Z_grid*np.exp(-np.reshape(chi,template_flux_grid.shape[1:])/2.0)) / np.sum(np.exp(-np.reshape(chi,template_flux_grid.shape[1:])/2.0))

                    if Instantaneous_Recycling:
                        full_template_mass_ira_grid = np.zeros(template_flux_grid.shape[1:])
                        for n in range(len(template_flux_grid[0,:,0,0,0])):
                            full_template_mass_ira_grid[n] = template_mass_ira_grid
                        weighted_average_mass_ira = np.sum(s*full_template_mass_ira_grid.ravel()*np.exp(-chi/2.0)) / np.sum(np.exp(-chi/2.0))

                chi.shape = template_flux_grid.shape[1:]
                s.shape = template_flux_grid.shape[1:]

                # Find the best fitting template
                arg_min = np.unravel_index(np.argmin(chi),chi.shape)

                #Change to force to correct metallicity if ForceCorrectZ = True
                if ForceCorrectZ:
                    Z_diff = abs(galform_mwZ[m]-use_template_Z_grid)
                    ClosestZind = np.argmin(Z_diff)
                    arg_min = np.unravel_index(np.argmin(chi[:,ClosestZind,:,:]),chi[:,ClosestZind,:,:].shape)
                    arg_min = (arg_min[0],ClosestZind,arg_min[1],arg_min[2])

                s_best_fit = s[arg_min]
                arg_min_mass = arg_min[1:] #This step is because the template stellar mass grid does not have an axis for dust attenuation
                sed_stellar_mass[m] = template_mass_grid[arg_min_mass] * s_best_fit    
                sed_remnant_mass[m] = template_mass_rem_grid[arg_min_mass] * s_best_fit
                sed_mass_weighted_age[m] = template_mass_weighted_age_grid[arg_min_mass]
                sed_age[m] = template_age_grid[arg_min[len(chi.shape)-2]]
                sed_tau[m] = use_tau_grid[arg_min[len(chi.shape)-1]]

                sed_Z[m] = use_template_Z_grid[arg_min[len(chi.shape)-3]]
                sed_s[m] = s_best_fit

                if WriteChiSquared:
                    sed_chisquared[m] = chi[arg_min]
                if WriteEBV:
                    sed_EBV[m] = EBV_grid[arg_min[0]]
                if Instantaneous_Recycling:
                    sed_mass_ira[m] = template_mass_ira_grid[arg_min_mass] * s_best_fit * (1-R)
                if Weighted_Average:
                    sed_weighted_average_mass[m] = weighted_average_mass
                    sed_weighted_average_Z[m] = weighted_average_Z
                    if Instantaneous_Recycling:
                        sed_weighted_average_mass_ira[m] = weighted_average_mass_ira * (1-R)
                if Write_SFR:
                    sed_sfr[m] = s_best_fit * np.exp(-sed_age[m]/sed_tau[m]) / (sed_tau[m]*10**9.0)

            if Output_Mode == "Default": # Output .npy files to separate directory

                extra_string = SFH_Type+'/'+SPS+'/'
                if not DustFit and not DustGalform and not UseCalzetti:
                    extra_string += 'nd/'
                elif not DustGalform and not UseCalzetti:
                    extra_string += 'dfit+ndgal/'
                elif not DustFit and not UseCalzetti:
                    extra_string += 'ndfit+dgal/'
                if UseCalzetti:
                    if len(extra_string) > len(SPS)+len(SFH_Type)+2 and extra_string[-1]=='/':
                        extra_string = extra_string.rstrip('/') + '+'
                    extra_string += 'Calzetti/' 
                if One_Metallicity:
                    if len(extra_string) > len(SPS)+len(SFH_Type)+2 and extra_string[-1]=='/':
                        extra_string = extra_string.rstrip('/') + '+'
                    extra_string += '1Z/'
                    model_backup = model
                    model += '_Z_'+str(template_Z_grid[Metallicity_ind])
                if ForceCorrectZ:
                    if len(extra_string) > len(SPS)+len(SFH_Type)+2 and extra_string[-1]=='/':
                        extra_string = extra_string.rstrip('/') + '+'
                    extra_string += 'ForceCorrectZ/'
                if Extra_Metallicity:
                    if len(extra_string) > len(SPS)+len(SFH_Type)+2 and extra_string[-1]=='/':
                        extra_string = extra_string.rstrip('/') + '+'
                    extra_string += 'ExtraZ/'
                if One_Tau:
                    if len(extra_string) > len(SPS)+len(SFH_Type)+2 and extra_string[-1]=='/':
                        extra_string = extra_string.rstrip('/') + '+'
                    extra_string += '1Tau/'
                if No_IRAC:
                    if len(extra_string) > len(SPS)+len(SFH_Type)+2 and extra_string[-1]=='/':
                        extra_string = extra_string.rstrip('/') + '+'
                    extra_string += 'NoIrac/'
                if No_NIR:
                    if len(extra_string) > len(SPS)+len(SFH_Type)+2 and extra_string[-1]=='/':
                        extra_string = extra_string.rstrip('/') + '+'
                    extra_string += 'NoNIR/'
                if sim is None:
                    output_path = '/gpfs/data/d72fqv/SED_Fitting/Fitted_Data/'+tree.lower()+'/'
                else:
                    output_path = '/gpfs/data/d72fqv/SED_Fitting/Fitted_Data/'+tree.lower()+'-'+sim+'/'

                np.save(output_path+extra_string+'z'+str(z)+'_'+model+'_sm',sed_stellar_mass)
                np.save(output_path+extra_string+'z'+str(z)+'_'+model+'_rm',sed_remnant_mass)
                np.save(output_path+extra_string+'z'+str(z)+'_'+model+'_mwage',sed_mass_weighted_age)
                np.save(output_path+extra_string+'z'+str(z)+'_'+model+'_age',sed_age)
                np.save(output_path+extra_string+'z'+str(z)+'_'+model+'_tau',sed_tau)
                np.save(output_path+extra_string+'z'+str(z)+'_'+model+'_Z',sed_Z)

                if WriteChiSquared:
                    np.save(output_path+extra_string+'z'+str(z)+'_'+model+'_ChiSquared',sed_chisquared)
                if WriteEBV:
                    np.save(output_path+extra_string+'z'+str(z)+'_'+model+'_EBV',sed_EBV)
                if Write_s:
                    np.save(output_path+extra_string+'z'+str(z)+'_'+model+'_s',sed_s)
                if Write_SFR:
                    np.save(output_path+extra_string+'z'+str(z)+'_'+model+'_sfr',sed_sfr)
                if Instantaneous_Recycling:
                    np.save(output_path+extra_string+'z'+str(z)+'_'+model+'_mass_ira',sed_mass_ira)
                if Weighted_Average:
                    np.save(output_path+extra_string+'z'+str(z)+'_'+model+'_weighted_average_mass',sed_weighted_average_mass)
                    np.save(output_path+extra_string+'z'+str(z)+'_'+model+'_weighted_average_Z',sed_weighted_average_Z)
                    if Instantaneous_Recycling:
                        np.save(output_path+extra_string+'z'+str(z)+'_'+model+'_weighted_average_mass_ira',sed_weighted_average_mass_ira)
                if One_Metallicity:
                    model = model_backup

            else: # Output to same directory that galform data was stored in.
                iz = z2snap(z,sim)
                output_path = galform_path + tree + "/" + sim + "/" + model +"/iz" + str(iz) + "/ivol" + str(subvolume[0]) + "/sed_fitting"
                
                if DustFit == False:
                    output_path += "_nodust"
                if No_IRAC:
                    output_path += "_noIRAC"
                if No_NIR:
                    output_path += "_noNIR"

                if output_format == "Default":
                    np.save(output_path+'_stellar_mass.npy',sed_stellar_mass)
                elif output_format == "ascii":
                    for_will = True
                    if for_will:
                        output_path = "/gpfs/data/d72fqv/SED_Fitting/Fitted_Data/ForWill/"
                        print subvolume, subvolume[0]
                        np.savetxt(output_path+'inferred_stellar_mass_'+model.replace(".","_")+"_iz_"+str(iz)+"_ivol_"+str(subvolume[0])+".dat",sed_mass_ira)
                        np.savetxt(output_path+'true_stellar_mass_'+model.replace(".","_")+"_iz_"+str(iz)+"_ivol_"+str(subvolume[0])+".dat",galform_mass)
                    else:
                        np.savetxt(output_path+'_stellar_mass.txt',sed_stellar_mass)
                        np.savetxt(output_path+'_true_stellar_mass.txt',galform_mass)                        
                else:
                    print "Error, output format, ", output_format, ", is not recognised"
                if Weighted_Average:
                    np.save(output_path+'_weighted_average_stellar_mass.npy',sed_weighted_average_mass)

# Plot mass[Fit]/mass[True] as a function of mass[True] as a sanity check/ notification that the code has finished running

import pylab as py
py.scatter(np.log10(galform_mass),np.log10(sed_stellar_mass/galform_mass),s=1,c='b',edgecolors='none')
#py.scatter(np.log10(galform_mass),np.log10(sed_mass_ira/galform_mass),s=1,c='b',edgecolors='none')
py.axhline(0.0)
py.show()
